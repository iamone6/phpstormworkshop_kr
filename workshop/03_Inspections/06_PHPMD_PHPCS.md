# PHP Mess Detector , PHP Code Sniffer 설정하기

> ### Open Settings Pane
> * Ctrl+Alt+S (Windows/Linux)
> * Command+, (Mac OS X)
>

PHP Mess Detector 또는 PHP Code Sniffer 플러그인을 검사 엔진으로 설정하기.

1. Settings 을 열고 _Languages and Frameworks | PHP_ 선택,  _[...]_ 를 클릭하여 interperter list 다음으로 이동하고 
    , "Visible only for this project" checkbox 를 해제하여  _PHPCS_ and _PHPMD_ interpreter 를 사용하게 한다.


### PHPCS configuration:
2. Open the Settings Pane then _Languages and Frameworks | PHP | Code Sniffer_. 
3. Click _[...]_ next to _Configuration_. 
4. Click _+_ and select "PHPCS".
5. Enter `phpcs` as the path for PHPCS. Validate installation.
6. Apply settings and open the Settings Pane then _Editor | Inspections_. Enable PHP Code Sniffer and set the code standard 
   to PSR-2.
7. Run PHP Code Sniffer Inspections on the current project.

### PHPMD configuration:
8. Open the Settings Pane then _Languages and Frameworks | PHP | Mess Detector_. 
9. Click _[...]_ next to _Configuration_.
10. Click _+_ and select "PHPCS". 
11. Enter `phpmd` as the path for PHPMD. Validate installation.
12.  Apply settings and open the Settings Pane then _Editor | Inspections_. Enable PHP Mess Detector and enable all the 
    rules to validate against.
13. Run PHP Mess Detector inspections on the current project.
