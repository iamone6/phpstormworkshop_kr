<?php
/**
 * Inspections - Settings
 *
 * Ctrl+Alt+S (Windows/Linux)
 * Command+, (Mac OS X)
 *
 * inspections 을 활성화/비활성화헌다, 예제 및 문서를 참고할 것.
 */

// 1. Settings pane 을 열고 Editor | Inspections 선택
// 2. Find the Expression Result Unused inspection and see why this is probably a code bug.
// 2. `Expression Result Unused` inspection 을 찾고 왜 이것이 코드의 버그로 취급되는지 확인한다.
// HINT: 검색으로 찾을 수 있다.
// 3. `Usage of Silence Operator` inspection 을 찾아서 Enable시켜 에러로 취급되게 하자. (silence operatior -> '@')
// 4. `Missing Break Statement` inspection 을 찾아서 error로 취급되게 하자.
// 5. `Inconsistent Return Points` inspection 을 disable 시키자.

namespace Inspections5\JetBrains;

@phpinfo();
