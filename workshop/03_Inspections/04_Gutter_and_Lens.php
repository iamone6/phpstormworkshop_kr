<?php
/**
 * Inspections - Gutter and Lens
 *
 * Right gutter displays error information. Use Lens mode for preview
 * 오른쪽 gutter 에서 에러 정보를 표시하고 있다. 해당영역은 Lens mode 로 미리보기한다.
 */

namespace Inspections4\JetBrains;

// 1. 에디터 오른쪽 사이드에서 error gutter를 찾아라.
// 2. 상단의 붉은 (!) 에 마우스를 올려 이 파일의 코드 퀄리티 요약본을 확인하라.
// 3. warning/error 정보를 작은 stripe 에 마우스를 올려 확인하라
// 4. Lens mode 를 보려면 아무스를 strip 에 올려라(해당영역이 화면에 없는 경우에만 표시됨).

class Navigation
{
    public function errorFunction()
    {
        echo 'First line'
        echo 'Second line';
    }

    public function warningFunction1()
    {
        $variable = true;
        foreach ($variable as $item) {

        }
    }

    public function warningFunction2()
    {
        $variable = 1;
        if ($variable = 2) {

        }
    }

    public function warningFunction3()
    {
        $variable = 1;
        switch ($variable) {
            case 1:
                echo 'Number one';
            case 2:
                echo 'Number two';
        }
    }

    public function deadCodeFunction1()
    {
        $variable = true;
    }

    public function deadCodeFunction2()
    {
        return true;

        if (true) {
            return false;
        }
    }

    public function spelling()
    {
        $mispelled = true;
    }
}
