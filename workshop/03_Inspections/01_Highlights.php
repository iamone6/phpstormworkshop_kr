<?php
/**
 * Inspections - Highlights
 *
 * 코드 내 이슈에 대한 하이라이팅
 *
 * NOTE: Inspections 은 PhpStorm 이 개발자에게 코드 내에서 무슨 일이 일어나고 있는지 알려주는 도구이다.
 */

namespace Inspections1\JetBrains;

class Highlights
{
    // 1. 아래 함수에서 에러가 붉은 언더라인으로 표시되고 있다.(missing semicolon).
    public function errorFunction()
    {
        echo 'First line'
        echo 'Second line';
    }

    // 2. 아래 함수에서는 워닝이 주황 언더라인으로 표시되고 있다 (foreach over boolean?).
    public function warningFunction1()
    {
        $variable = true;
        foreach ($variable as $item) {

        }
    }

    // 3. settings (Editor | Inspections | PHP | Probable bugs) 에서 "Assignment in condition" inspection을 enable하자.
    //    이렇게 하면 if 구문 내에서 '=' 으로 입력된 곳은 warning 이 나게 된다.
    //    HINT : 설정 숏컷이 존재한다. Ctrl+Alt+S (Windows/Linux) Command+, (Mac OS X)
    public function warningFunction2()
    {
        $variable = 1;
        if ($variable = 2) {

        }
    }

    // 4. 아래 함수에서 warning highlight 를 확인할 수 있다. (missing break statement) - 하지만 이것은 제안일 뿐이다.
    public function warningFunction3()
    {
        $variable = 1;
        switch ($variable) {
            case 1:
                echo 'Number one';
            case 2:
                echo 'Number two';
        }
    }

    // 5. Dead code (unused variable) 의 경우
    public function deadCodeFunction1()
    {
        $variable = true;
    }

    // 6. Dead code (unreachable statement)의 경우 두번째
    public function deadCodeFunction2()
    {
        return true;

        if (true) {
            return false;
        }
    }

    // 7. warning highlight 가 오타난 변수인 "$mispelled" 에 보인다.
    //    Suppress the inspection for this statement only using Show Intention Actions |  Typo rename to...
    //         | Suppress for statement
    public function spelling()
    {
        $mispelled = true;
    }
}
