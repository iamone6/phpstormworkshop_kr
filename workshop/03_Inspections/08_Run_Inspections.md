# Run Inspections

> ### Next/Previous Problem 으로 이동하기
> 
> * Ctrl+Alt+Up / Down (Windows/Linux)
> * Alt+Command+Up / Down (Mac OS X)

> inspection profile 을 실행하고 전체 프로젝트(나 특정 범위) 에 대한 결과를 확인한다.

1. _Code_ 메뉴에서 _Inspect Code..._ 를 선택한다.
2.  _Whole project_ 으로 inspection 을 실행한다.
3. 몇개의 inspection 만 검사하도록 사용자정의 _Inspection profile_ 을 만든다.
   
   HINT: _Inspection profile_ 옆의 _[...]_ 를 사용해서 선택된 프로파일에서 inspection 을 활성/비활성화 한다.
    
4. tool window 에서 그 결과를 확인한다.
5. Group by severity 아이콘을 사용하여 오류별로 그룹화한다.
6. 단축키로 이동해 본다.
7. inspections 을 HTML로 export한다. (아이콘 확인)
