<?php /** @noinspection PhpUnusedLocalVariableInspection */

/**
 * Inspections - Find Issues
 *
 * 유사 이슈들을 검색하는 Quickfix menu
 */

namespace Inspections6\JetBrains;

class FindIssues
{
    // 1. 아래 함수에서 warning 이 보일 것이다 (foreach over boolean?).
    //  Quickfix (Alt+Enter) 를 열고 전체 프로젝트에 대한 inspection 을 실행해 보자.
    //  새로운 도구 윈도우가 열리고 유사 에러가 프로젝트에서 검색되어 보여진다.
    // HINT: Inspection Options menu (top item) 에서 오른쪽 방향키를 사용하여 Run Inspection on... 을 선택.

    public function warningFunction()
    {
        $variable = true;
        foreach ($variable as $item) {

        }
    }

    // 2. Dead code 이다.(unused variable). 파일에 대해 inspection 을 실행해 보자

    public function deadCodeFunction()
    {
        $variable = true;
    }
}
