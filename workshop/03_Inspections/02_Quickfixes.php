<?php
/**
 * Inspections - Quickfixes
 *
 * Alt+Enter (Windows/Linux/Mac OS X)
 *
 * 하이라이트 상황에서 경고를 없앨 수 있도록 Quick Fix 를 제공한다.
 */

namespace Inspections2\JetBrains;

class QuickFixes
{
    // 1. 오타가 난 변수 `$mispelled` 위에 warning 이 보이고 있다.
    //    caret 을 변수에 위치시키고 Quickfix 로 오타를 잡자.
    // HINT: Select the default Typo: Rename to... using Enter then select the change using Enter again.


    public function spelling()
    {
        $mispelled = true;
    }

    // 2. class Person 에 대해 알수가 없기 때문에 워닝이 나고 있다.
    //    caret을 Person에 위치시키고 Quickfix 로 클래스를 import 하자
    public function importClass()
    {
        $person = new Person();
    }

    // 3. 미정의 변수 $this->_variable 가 사용되었다.
    //    caret 을  "_variable" 에 두고 Quickfix 로 클래스에 추가해 보자.(Add field)
    public function undefinedVariable()
    {
        $this->_variable = 'test';
    }
}
