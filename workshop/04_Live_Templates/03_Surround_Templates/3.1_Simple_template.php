<?php
/**
 * Creating Live Templates - Surround Template
 *
 * Surround With
 *
 * Ctrl+Alt+T (Windows/Linux)
 * Alt+Command+T (Mac OS X)
 *
 * Surround With Live Template
 *
 * Ctrl+Alt+J (Windows/Linux)
 * Alt+Command+J (Mac OS X)
 *
 * 코드를 템플릿으로 감싼다. 템플릿에서 $SELECTION$ 으로 표시되는 부분이 선택된 코드 부분이다.
 */

namespace LiveTemplates3\JetBrains;

/*
1. Setting Pane -> Editor | Live Templates 에서 새 템플릿을 만들자.
    이름은 "trycatch"

   다음 문장을 입력하자:

try {
    $SELECTION$
} catch ($TYPE$ $$$VARIABLENAME$) {
    $END$
}

    variable 대화상자에서 변수들의 순서를 TYPE 이 맨 앞으로 오게 설정하고 표현은 complete() 으로 설정하자.
   HINT: 기본값을 "Exception" (따옴표 포함) 으로 입력할수도 있다. VARIABLENAME 도 "exception" (따옴표 포함)으로 설정할 수 있다.

    라이브 템플릿을 저장하자.

2. 테스트 해 보자. 아래 코드라인을 선택하고 라이브템플릿으로 감싸 보자
*/
