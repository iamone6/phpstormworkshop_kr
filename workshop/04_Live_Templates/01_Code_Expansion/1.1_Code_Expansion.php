<?php
/**
 * Code Expansion
 *
 * Tab (Windows/Linux/Mac OS X)
 *
 * Expand text shortcut using template.
 * Expands into code with variable “hotspots”.
 * Hotspot can be linked to a macro, such as “suggest type of variable”, “suggest name for variable”, “clipboard content”,
 *     “new GUID”, etc.
 * Tab to move to next hotspot.
 * Template can define end point for caret.
 */
/*
 * Code Expansion
 *
 * Tab (Windows/Linux/Mac OS X)
 *
 * 템플릿을 사용하여 text shortcut을 확장하세요
 * "hotspot" 변수를 사용하여 코드를 추가 입력하세요.
 * "hotspot"은 "suggest type of variable", "suggest name for variable", "clipboard content" , "new GUID" 등등과 같은
 * 매크로와 연동될 수 있습니다.
 * tab을 눌러 다음 hotspot 으로 이동하세요.
 * 템플릿은 caret 의 엔드포인트를 정의할 수 있습니다.
 *
 * 주) live template : 축약어 + tab 으로 일정 포멧의 출력을 정의된 템플릿(==live template)에 따라 에디터에 넣어 줌.
 *  이때 직접 입력할 위치(==hotspot)에 커서가 이동되고 tab으로 그 위치를 옮겨다닐 수 있음.
 */
namespace LiveTemplates1\JetBrains;

// 1. Setting Pane -> Editor | Live Templates 에서 사용이 가능한 다양한 라이브 템플릿들을 확인해 보세요
//    이 메뉴에서 라이브 템플릿의 사용 여부를 결정하거나 직접 만들 수 있습니다.
// HINT: 파일을 편집하는 위치에 따라서 서로 다른 라이브 템플릿이 적용됩니다.

class CodeExpansion
{
    // 2. protected function firstFunction() 을 만들어 봅시다.
    //    `prof`을 치고 TAB을 치세요. hotspot 에 firstFunction 이라 입력하고 TAB 을 다시 치세요.
    //    함수에 인자를 추가할 수도 있고 그냥 둘 수도 있습니다. TAB을 다시 쳐서 함수의 안으로 들어가세요
    // prof <TAB>

    // 3. public static function staticFunction1() 을 다음처럼 입력하여 pubsf 라이브 템플릿을 써서 만들어 보세요.
    // pubsf <TAB>

    protected function secondFunction()
    {

        $numbers = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
        /** @var integer $number */

        // 4. $number 배열을 순회해 보자. "fore" +TAB 을 입력하자.
        //    첫번째 hotspot에서 $numbers 를 입력하고 TAB을 치차.
        //    두번째 hotspot 에서 기본 변수명을 사용하기로 하고 TAB을 입력하여 순회 본체로 들어가자.
        // fore <TAB>

    }
}
