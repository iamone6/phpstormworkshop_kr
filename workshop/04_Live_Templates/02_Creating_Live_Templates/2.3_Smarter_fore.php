<?php
/**
 * Creating Live Templates
 */

namespace LiveTemplates2\JetBrains;

/*
1. Setting Pane -> Editor | Live Templates 에서 새 live template 을 만들어 보자
    이름은 "fe" 로 짓는다 - 이전 템플릿과 비슷하지만 더욱 영리한 템플릿이 될 것이다.

    텍스트로는 다음을 입력한다:

foreach ($ITEMS$ as $ITEM$) {
    $END$
}

    문장을 설정하면 라이브 템플릿이 PHP에서 사용 가능해 진다
    Reformat according to style 도 설정할 수 있다.

    이제 Edit Variables 버튼을 클릭하자. 여기에서 template hotspot 의 기본값을 명시할 수 있다.
    ITEMS 의 기본값으로 complete() 라고 입력하자.
    ITEM 의 기본값은 "$item" 이라 입력하자 (따옴표까지 입력이다).

    이제 live template 을 저장한다.

2. 이제 새 템플릿을 실험해 보자. IDE 의 autocompletion 을 사용함으로 어떻게 더 똑똑해 졌는지 보자.
*/

$numbers = array(1, 2, 3, 4, 5, 6, 7, 8, 9);

// fe <TAB>
