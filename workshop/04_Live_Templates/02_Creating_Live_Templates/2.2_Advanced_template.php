<?php
/**
 * Creating Advanced Live Templates
 */

namespace LiveTemplates2\JetBrains;

/*
1.Setting Pane -> Editor | Live Templates 에서 새로운 템플릿을 등록하자. 이름은 "prop"이다.
    템플릿에 넣을 코드는 AdvencedTemplate.txt 에 있다. 변수가 사용되는 방법을 주목하자. ( $$은 PHP의 $ 를 escape 하는데 쓰였다)

    이 문맥을 live template 에 설정하면 PHP에서 사용 가능하다.
    Reformat according to style 또한 가능하다.

    Edit Variable 버튼을 클릭하자. 여기에서는 template hotspot 에서 기본값으로 넣어줄 내용을 명시할 수 있다.
    PROPERTY_NAME 을 대문자로 FIELD_NAME  라고 설정하자.
    이제 자동으로 PROEPRTY_NAME 변수의 위치에는 FIELD_NAME에서 입력한 내용이 들어가서 나타날 것이다.

    다른 템플릿들에서도 물론 사용이 가능하다, 예를 들어 현재 user name, date, time 등등을 쓸 수 있다. autocompletion도 지원된다.

    이제 live template 을 저장하자.

2. "prop" 이라는 새 라이브 템플릿을 테스트 해 보자
    $TYPES$ 와 $FIELD_NAME$ 변수가 hotspot 이다.
*/

class LiveTemplates
{

    // prop <TAB>


}
