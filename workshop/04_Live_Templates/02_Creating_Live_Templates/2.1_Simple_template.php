<?php
/**
 * Creating Simple Live Templates
 */

namespace LiveTemplates2\JetBrains;

/*
1.Setting Pane -> Editor | Live Templates 에서 새로운 live template 을 만들자.
   HINT: 우측 [+] icon
    이름을 "cls"로 짓고
    아래 텍스트를 template 으로 설정하자

class $CLASS_NAME$ {
    $END$
}

    이 문장을 live template 으로 설정하면 PHP에서 사용이 가능해 진다.
    스타일에 따른 Reformat 이 가능해 진다.
    live template 을 저장하자.
    * 역주: 이때 template text 하단에 범위를 PHP로 반드시 설정하자. 범위를 설정하지 않으면 동작하지 않는다.

2. 새로 만든 cls 템플릿을 테스트하자.
   $CLASS_NAME$ 변수는 hotspot이 된다.
   $END$ 변수는 IDE 에게 템플릿을 넣은 후 커서를 어디에 위치할지를 알려주는 특별한 마커이다.
*/

// cls <TAB>
