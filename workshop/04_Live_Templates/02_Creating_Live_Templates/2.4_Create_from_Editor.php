<?php
/**
 * Creating Live Template From Editor
 */

namespace LiveTemplates2\JetBrains;

// 1. 아래 두 라인을 선택한 상태로 만들고,
echo "$TEXT$";
$END$

// 2. Use the Tools | Save as Live Template... menu.
// 3. Save the template as 'e'.
// 4. Test the template.

// e <TAB>

