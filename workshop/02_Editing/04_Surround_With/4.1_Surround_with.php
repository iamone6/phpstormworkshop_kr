<?php
/**
 * Surround With
 *
 * Ctrl+Alt+T (Windows/Linux)
 * Alt+Command+T (Mac OS X)
 *
 * Show Intention Actions
 *
 * Alt+Enter (Windows/Linux/Mac OSX)
 *
 * 선택된 텍스트를 둘러쌀 수 있는 관련 내용 (e.g. try/catch or if statement) 으로 감싼다.
 * NOTE: Show Intention Actions 을 사용해도 상황에 따라 둘러쌀 수 있는 내용들을 호출할 수 있다.
 */

namespace Editing4\JetBrains;

class SurroundWith
{
    // 1. 함수 내의 아래 구문을 try/catch 블럭으로 감싸라
    public function surroundWithTryCatch()
    {
        echo 'This may be throwing an Exception. Riiiight!';
    }

    // 2. $items[0] element에 carnet 을 위치시키고. Alt+Enter 를 사용하여 if(!empty(....)) 검사로 감싸라.
    public function division($items)
    {
        return $items[0] / $items[1];
    }
}
