<?php
/**
 * Column Selection Mode
 *
 * Alt+Shift+Insert (Windows/Linux)
 * Shift+Command+8 (Mac OS X)
 *
 * 칼럼 선택방법을 토글한다 (한번에 여러줄을 편집할 수 있는 기능을 on/off 한다)
 */

namespace Editing2\JetBrains;

class ColumnSelection
{
    // 1. Column selection Mode 를 활성화 한 다음에
    // 2. $someVariable column을 모두 감싸는 사각형을 마우스로 그린다.
    // 3. 변수명을 $foo로 한번에 변경한다
    // 2. Column Selection Mode 를 끈다.$someVariable
    
    public function columnSelection()
    {
        $someVariable = 3;
        $someVariable = 6;
        $someVariable = 9;
        $someVariable = 'a';
        $someVariable = 'b';
        $someVariable = 'c';

    }
}
