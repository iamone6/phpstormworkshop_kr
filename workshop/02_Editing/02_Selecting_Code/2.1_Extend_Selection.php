<?php
/**
 * Extend and Shrink Selection
 *
 * Ctrl+W / Ctrl+Shift+W (Windows/Linux)
 * Alt+Up / Alt+Down (Mac OS X)
 *
 * Extend (or Shrink) the selection to the next logical boundary
 * 선택 영역을 순차적으로 확대(/축소) 한다.
 */

namespace Editing2\JetBrains;

use Exception;

class SelectingCode
{
    // 1. $importantValue 에 carnet 을 배치하고 Extend Selection 을 한다. 그러면 해당 변수가 선택된다.
    //    한번 더 반복하면 ; 가 빠진 구문이 선택된다.
    //    한번 더 반복하면 ; 가 포함된 구문이 선택된다.
    //    한번 더 반복하면 한줄이 선택된다.
    //    한번 더 반복하면 함수의 body 가 선택된다.
    //    한번 더 반복하면 함수 전체가 선택된다
    //    한번 더 반복하면 클래스 전체가 선택된다
    //    한번 더 반복하면 전체 네임스페이스가 섲택된다
    //    한번 더 반복하면 젅체 파일이 선택된다.
    // 2. 이제 Shrink Selection 으로 extendAndShrinkSelection()  함수 전체가 선택될때 까지 축소해 보자


    /** @var string */
    public $Name;
    /** @var int */
    public $Age;

    public function extendAndShrinkSelection()
    {
        $importantValue = 32;
        if ($importantValue > 42) {
            try {
                echo 'More important than 42?';
            } catch (Exception $ex) {
                // 3. 커서를 $ex 변수에 놓고 확장과 축소 선택을 해 보자
                echo $ex->getMessage();
            }
        }
    }

    public function run()
    {
    }

    public function sayHello()
    {
    }

    public function sayGoodbye()
    {
    }

    public function veryLongFunctionName()
    {
    }
}
