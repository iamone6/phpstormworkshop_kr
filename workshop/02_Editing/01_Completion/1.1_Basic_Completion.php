<?php
/**
 * Basic Completion
 *
 * Ctrl+Space (Windows/Linux/Mac OS X)
 *
 * Ctrl+Shift+Enter to complete statement (Windows/Linux)
 * Shift+Command+Enter to complete statement (Mac OS X)
 *
 * Basic (code) completion for the name of any class, method or variable.
 * 클래스명, 메소드명, 변수명에 대한 기본적인 코드 완성
 */

namespace Editing1\JetBrains;

class BasicCompletion
{
    /** @var string */
    public $Name;

    /** @var int */
    public $Age;

    public function basicCompletion()
    {

        // 1. `$this->` 를 입력하고, Basic Completion 을 사용한다.
        //    Basic Completion 은 현재 문맥상에서 사용할수 있는 코드 완성 옵션들을 보여준다.
        //    현재 타입의 멤버들은 bold 되어 표시된다.
        //    더 많은 내용을 부려면 한번 더 Basic Completion 을 사용하라.
        //    스페이스바, 탭, 엔터를 사용하여 선택을 적용할 수 있다. $this->Name 이 되도록 선택하자.
        // $this->
        $this->sayGoodbye();
        $this->veryLongFunctionName();
        $this->veryLongFunctionName();

        // 2. `$value =` 를 입력하고, Basic Completion 을 사용하자.(`=` 부호 뒤에서 사용)
        //     Completion 은 로컬 범위의 선택 가능한 옵션을을 표시하고 난 뒤, 뒤쪽에 더 넓은 범위의 옵션을 표시하게 된다.
        //     보통 가장 자주 사용된 아이템이 미리 선택된 채로 표시된다.
        //      예: $this->Age 가 가장 많이 사용되었다면 맨 위에 나타날 것이다.
        //      ESC를 눌러 이 윈도우를 없앤다.
        //$value =


        // 3. `$this->r` 을 입력하고 Complete 가 구문 완성을 사용하는 동작을 살펴보자.
        //    Complete the statement using the Complete Statement.
        // $this->r
        $this->run();


        // 7. `if (true` 까지 입력하면 Complete Statement 를 사용하여 구문이 완성된다.
        // if (true
        if (true)
        if (true)


        // 8. "say" 다음에 caret 을 위치시키고 Basic Compoetion 을 사용하여
        //  sayHello() 메소드를 sayGoodbye() 메소드로 변경하자.
        //     NOTE: Tab 은 구문을 변경하고, enter 는 구문을 끼워넣는다.
        $this->sayHello();
        $this->sayGoodbye();


        // 9. CamelHumps 를 사용하여 veryLongFunctionName() 을 불러와 보자.
        //    다음을 입력: $this->
        //    "vlfn" 을 타이핑하여 함수를 찾는다.
        // $this->
        $this->veryLongFunctionName();

        // 10. path completion을 해보자.
        //  Code/ 뒤에 caret 을 위치시키고 basic completion 을 실행하여
        //  "Code/01_Simple_Include.php" 파일을 인클루드하도록 해 보자
        require_once 'Code/';



    }

    public function sayHello()
    {
    }

    public function run()
    {
    }

    public function sayGoodbye()
    {
    }

    public function veryLongFunctionName()
    {
    }
}
