<?php
/**
 * Basic Completion With Import
 *
 * Ctrl+Space (Windows/Linux/Mac OS X)
 *
 * Ctrl+Shift+Enter to complete statement (Windows/Linux)
 * Shift+Command+Enter to complete statement (Mac OS X)
 *
 * 현재 파일의 네임스페이스 바깥에서 온 클래스나 함수를 basic completion 을 사용하여 불러오면,
 * `use` 구문이 자동으로 추가될 것이다.
 */

namespace Editing1\JetBrains;

// 1. ICustomer 를 implement 하는 구문을 만들자(caret 을 implements 뒤에 위치시키고 basic completion 실행)
//         `use Navigation4\JetBrains\Customers\ICustomer;`  도 추가될 것이다.
//
use Refactoring13\JetBrains\Person;

class ImportCompletion implements
{
}
class a extends Person{

}