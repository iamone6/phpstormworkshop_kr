<?php
/**
 * Type Hinting
 *
 * 당신이 작업하는 type 을 PhpStorm 에게 알려준다.
 *
 * Quick Documentation
 *
 * Ctrl+Q (Windows/Linux)
 * F1 (Mac OS X)
 *
 * 변수, 클래스, 메소드에 대해 빠른 문서정보를 제공한다.
 */

namespace Editing1\JetBrains;

use Debugging\JetBrains\Person;

$people = array();
$people[] = new Person('Trillian');

foreach ($people as $person) {
    // 1. $pseson 에 대한 자동완성기능이 작동하지 않을 것이다.
    //      직접 $person->을 입력하여 확인해 보라.
    // $person->

    // 2. 이 문제를 해결해 보자.
    //    19번 라인의  $people = array(); 의 윗줄에 PHPDoc comment 를 추가한다:
    //        /** @var $people Person[] */
    //    이렇게 하면 PhpStorm 에게 이 배열이 Person 객체를 담고 있다고 알려주게 된다.
    //    이제 $person-> 을 다시 입력하여 코드완성이 뜨는 것을 확인해 보자
    // $person->

    // 3. 커서를 $person 에 두고 Quick Documentation 을 실행한다.

    // 4. Person 클래스에 커서를 두고 Quick Documentation 을 실행한다.

    // 5. Perspn 클래스로 이동하여 PHPDoc annotation 을 클래스에 추가하자.
    //      몇몇 @property annotation 을 추가하자 예를들어:
    //        @property string Address Address of the person
    //        @property string Country Person country
    //      PhpStrom은 이 프로퍼티가 실제로 존재하지 않더라도 이 annotation 을 파싱하여
    //      Person 클래스의 completion 을 사용할 때 Address 와 Country를 제공하게 될 것이다.
    //      `$person->A` 를 타이핑해서 실제로 실험해 보자.
    //
    //    노트 :  PhpStorm 은 다음 annotation도 지원한다 : @method, @type, @property, @deprecated, @global, @name, @param, @static, @since, @link, @mixin, ...
    // $person->A


}
