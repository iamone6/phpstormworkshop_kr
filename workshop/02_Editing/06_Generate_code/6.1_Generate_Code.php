<?php
/**
 * Generate Code
 *
 * Alt+Insert (Windows/Linux)
 * Command+N (Mac OS X)
 *
 * 코드를 생성한다 (class members, constructor, PHPDoc docblock comments, fields etc)
 */

namespace Editing6\JetBrains;

// 1. Person 클래스 내부 어딘가에 커서를 위치한다..
// 2. Generate Code 를 실행하고 overriding method 를 사용하여 setId() method 를 만들자.
// 3. 생성자를 만들자.
// 4. 두 프로퍼티를 사용하는 getter, setter 를 만들자.
// 5. Person class를 위한 PHPDoc docblock 을 만들자.
// 6. __toString 메소드를 만들어 메소드를 implement 하자.
// 7. isTeenager() 메소드 내 $_age 프로퍼티에 caret 을 위치시키고 alt+enter 로 $_age 라는 private field를 생성(add filed)하자.


class Person extends Entity
{
    protected $_firstName;
    protected $_lastName;

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->_firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void
    {
        $this->_firstName = $firstName;
    }




    public function isTeenager()
    {
        return $this->_age > 10;
    }

    // here is a good place to start

}

/**
 * Class Entity
 * @package Editing6\JetBrains
 */
class Entity
{
    /**
     * @var int
     */
    protected $_id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }
}
