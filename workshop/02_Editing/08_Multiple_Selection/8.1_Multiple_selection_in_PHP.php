<?php
/**
 * Multiple Selections in PHP
 *
 * Create Multiple Cursors
 *
 * - Alt 를 누른채로 마우스를 사용하여 커서가 필요한 위치들을 선택한다.(Windows/Linux/Mac OS X)
 *
 * Revert to One Cursor
 *
 * - Escape (Windows/Linux/Mac OS X)
 *
 * Select Next Occurrence
 *
 *   - Alt+J (Windows/Linux)
 *   - Ctrl+G (Mac OS X)
 *
 * 커서를 파일의 여러곳에 위치시키고 코드를 동시에 작성한다.
 */

// 1. 아래는 전부  plain text, 우리는 모든 리소스를 배열에 담는 작업을 할 것이다.
//    첫 라인과 빈 라인을 지워서 실제 리소스들만 남도록 한다.
// 2. 커서를 첫 `*` 바로 뒤에 배치시키고 Alt 와 마우스를 사용하여 원하는 곳에 커서를 배치한다. 모든 `*` 바로 뒤에 커서들을 배치하자.
// 3. ALt 에서 손을 때고 입력을 시작한다. 텍스트는 모든 커서에서 동일하게 나타나게 된다. 백스페이스를 사용하여 모든 `*`을
//    삭제하고 각 라인의 앞에 `'` 을 추가해 주자.
//    커서를 각 라인의 끝으로 옮긴 뒤 `'`` 를 추가하자.
// 4. Esc 를 눌러 커서를 하나로 되돌려 놓고, 첫번째 라인의 | 를 선택하도록 하자. 나머지 모든 | 를 선택하기 위해 Alt+J 를 누르자.
//    이제 |를 한꺼번에 삭제하고 ` ', '` 를 입력하자.
//    커서를 라인의 끝으로 이동시켜 delete 키를 두르자. 모든 리소스를 한개의 라인으로 합쳐줄 것이다.
// 5. Esc 로 커서 한개로 되돌아온 뒤 맨 앞에 array( 를 붙여주고 맨 끝에는 ); 를 붙여주면 배열 정의가 끝난다.
// 6. Alt 를 누른채로 마우스를 드래그하여 bumber 6 부터 // 1..의 앞까지 선택하라.
//    그리고 다중선택이 여러개의 커서가 동시에 추가시킨 것을 확인해 봐라.

PhpStorm Resources

array('Blog','http://blog.jetbrains.com/phpstorm',);
array('Twitter','http://twitter.com/phpstorm',);
array('Video tutorials','http://www.jetbrains.com/phpstorm/documentation/phpstorm-video-tutorials.jsp',);
array('Tutorials','http://confluence.jetbrains.com/display/PhpStorm/Tutorials',);