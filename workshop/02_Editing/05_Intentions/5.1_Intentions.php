<?php
/**
 * Intentions
 *
 * Show Intention Actions (aka Quickfixes)
 *
 * Alt+Enter (Windows/Linux/Mac OS X)
 *
 * IDE가 context로부터 액션을 추측하여 실행한다.
*/

namespace Editing5\JetBrains;

class Intentions
{
    // 1.  '===' 뒤집기를 실행하라. 커서를 if 구문의 $variable 에 위치시키고
    //    Show Intention Actions (also known as Quickfixes) 을 실행한다.
    //    이제 `$variable === true` 을 뒤집어`true === $variable` 로 만들 수 있따.

    public function flipIntention()
    {
        $variable = true;
        if ($variable === true) {

        }
        echo 'This may be throwing an Exception. Riiiight!';
    }

    // 2. File intention 을 실행한다. `nonexistingfile.php` 에 carnet을 위치시키고 Quickfixes 를 실행한다.
    //    Create File intention 을 실행하여 파일을 생성한다.
    public function createFileIntention($items)
    {
        include 'nonexistingfile.php';
    }

    // 3. Boolean Expression Intention 으로 풀린 표현식을 축약하자. 함수의 마지막 구문으로 caret 을 이동하고
    //    intension 을 사용하자.
    public function simplifyIntention()
    {
        $a = 0;
        $b = 1;
        echo $b || !$a;
    }
}
