<?php
/**
 * Reformat Code
 *
 * Ctrl+Alt+L (Windows/Linux)
 * Alt+Command+L (Mac OS X)
 *
 * 코드 스타일에 따라 모든 코드를 재배치하고, 옵션으로 설정에 따라 항목들도 재배치한다,.
 *
 * Show Reformat Code Dialog
 *
 * Shift+Ctrl+Alt+L (Windows/Linux)
 * Shift+Alt+Command+L (Mac OS X)
 *
 * 다이얼로그와 함께 코드를 Reformat/rearrange 한다.
 *
 */

namespace Editing7\JetBrains;

use Editing2;
use Editing6;

// 1. 아래의 클래스는 엉망이다.
//    - Public/protected/static 멤버들이 섞여있다.
//    - Brackets 이 일관성이 없다.
//    - Indents 가 일관성이 없다.
// 2. 메뉴에서 Code | Rearrange Code 를 선택하여 아래 클래스를 재구성하라.
// 3. 이전으로 돌아가려면 Undo 를 사용하라.
// 4. 사용되지 않는 import 도 보인다.(Editing2 and Editing6).
//    메뉴에서 Code | Optimize Imports 를 사용하여 이 문제를 해결하라.
// 5. Optimize Imports action 이전으로 되돌리려면 Undo를 사용해라.
// 6. 단축키를 사용하여 Reformat Code 를 사용하면,  Rearrange Code 와 Optimize Imports 를 한번에 실행한다. (Show Reformat Code 다이얼로그도 실행된다.)
//    NOTE: Reformat Code 전체 프로젝트를 대상으로 실행할 수 있다.
// 7. Settings 을 열고 Editor | Code Style | PHP 로 이동한다.
//      Arrangement tab에서, 필드가 visibility 로 정렬되고,다음 이름으로 정렬되도록 하자.
//      새로운 설정을 테스트하기 위해 Rearrange Code 를 실행해 보자.

class Rearrange
{
    protected $_firstProperty;
    public $_secondProperty;
    public static $_thirdProperty;
    protected $_fourthProperty;

    public function firstFunction()
    {
        for ($i = 0; $i < 10; $i++) {
            echo 'Test';
        }
    }

    public function setFirstProperty($firstProperty)
    {
        $this->_firstProperty = $firstProperty;
    }

    public function getFirstProperty()
    {
        return $this->_firstProperty;
    }

    public function setSecondProperty($secondProperty)
    {
        $this->_secondProperty = $secondProperty;
    }

    public function getSecondProperty()
    {
        return $this->_secondProperty;
    }

    public static function setThirdProperty($thirdProperty)
    {
        self::$_thirdProperty = $thirdProperty;
    }

    public static function getThirdProperty()
    {
        return self::$_thirdProperty;
    }

    public function setFourthProperty($fourthProperty)
    {
        $this->_fourthProperty = $fourthProperty;
    }

    public function getFourthProperty()
    {
        return $this->_fourthProperty;
    }
}
