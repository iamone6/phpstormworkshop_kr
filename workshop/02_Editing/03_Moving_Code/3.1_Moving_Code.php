<?php
/**
 * Moving Code
 *
 * Move Line Up/Down:
 *
 * Shift+Alt+Up / Shift+Alt+Down (Windows/Linux/Mac OS X)
 *
 *  라인 한 줄을 위아래로 이동한다.
 *
 * Move Statement Up/Down
 *
 * Shift+Ctrl+Up / Shift+Ctrl+Down (Windows/Linux)
 * Shift+Command+Up / Shift+Command+Down (Mac OS X)
 *
 * 선택된 구문 전체를 위아래로 이동한다
 */

namespace Editing3\JetBrains;

class MovingCode
{
    // 1. move line up / down 을 사용하여 순서대로 정렬시켜 보자
    public function moveLine()
    {
        $numberOne = 1;
        $numberTwo = 2;
        $numberThree = 3;
        $numberFour = 4;
        $numberFive = 5;
    }

    // 2. move statement up / down 으로 if 구문을 정렬해 보자
    public function moveStatement()
    {
        if (2 == 2) {
            // ...
            echo 'Number two';
        }
        if (1 == 1) {
            // ...
            echo 'Number one';
        }
        if (3 == 3) {
            // ...
            echo 'Number three';
        }
    }
}
