<?php
/**
 * Debugging Web Application
 *
 * 추가적인 설정 없이 디버깅을 사용한다.
 */

namespace Debugging7\JetBrains;

// 0. docker-compose.yml 에 정상적인 IP가 설정되어 있는지 확인한다.
// 1. "docker-compose up" 이 되어있는지 확인한다.
// 2. http://www.jetbrains.com/phpstorm/marklets/ 로 가서 Zend Debugger 나 Xdebug 같은 디버거를 bookmarklets 한다.
//   Start Debugger bookmarklet 을 bookmarks bar 로 드래그해 온다.
//
//    심플한 브라우저 extension 을 더 좋아한다면 http://confluence.jetbrains.com/display/PhpStorm/Browser+Debugging+Extensions 을 참조하여
//    이중 하나를 설치하자.
//    크롬을 예로 들어본다면 https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc 를 설치한다.
//
// 3. http://localhost:8081/07_Debugging_Web_Application.php 을 열거나, 에디터 우상단의 브라우저 아이콘을 클릭한다.
// 4. PhpStorm 에서  Run | Start Listening 으로 PHP Debug Connections 을 시작한다.
// 5. 브라우저에서, Start Debugger bookmarklet 을 사용한다.  "Show Greeting" 을 클릭하고 PhpStorm 이 새로운 debug connection 을 만드는지 확인한다.
//    브레이크포인트를 생성하고 Debug Tool Window 에서 global 변수들($_COOKIE, $_GET, $_REQUEST)을 포함한 변수들을 추적한다.
?>
<!DOCTYPE html>
<html>
<head>
    <title>Web Application</title>
</head>
<body>
<form>
    <label for="name">Name:</label>
    <input type="text" name="name" id="name"/>
    <input type="submit" value="Show Greeting"/>
</form>
<?php
if (isset($_REQUEST['name'])) {
    echo '<h2>Hello, ' . htmlentities($_REQUEST['name']) . '</h2>';
}
?>
</body>
</html>
