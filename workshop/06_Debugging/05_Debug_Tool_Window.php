<?php
/**
 * Debug Tool Window
 *
 * 디버그 세션중에 실행 상세사상, 변수 등을 살펴볼 수 있도록 해주는 윈도우.
 * 실행중에 변수값을 변경할 수 있다.
 *
 * Alt+5 (Alt+F8 evaluate expression) (Windows/Linux)
 * Command+5 (Alt+F8 evaluate expression) (Mac OS X)
 */

namespace Debugging3\JetBrains;

require_once 'Code/Person.php';

use Debugging\JetBrains\Person;

$name = 'Maarten';
$fruits = array('apple', 'pear', 'lemon');

// 1. Person 클래스의 생성자로 이동하여 breakpoint 룰 둔다.
$person = new Person($name);

foreach ($fruits as $fruit) {
    // 2. 아래 라인에 브레이크포인트를 추가한다.
    echo $person->getName() . " likes " . $fruit . "\r\n";
}

// 3. context menu 로 PHP 스크립트를 디버그하자. 디버거가 실행되고 Person.php 에서 첫번째 브레이크가 발생한다.
//    Debug Tool Window의 왼쪽에 프레임이 표시된다. 다시말해 코드의 몇번쨰 라인에서 코드의 몇번째 라인이 호출되었나 하는 내용이다.
//    Person.php 파일은 현재 파일에서 호출되었음이 확인된다. 큰 어클리케이션에서는 실행 스택을 확인할 수 있는 곳이 된다
// 4. 프레임 리스트의 "05_Debug_Tool_Window.php" 를 클릭하면 Person constructor 가 호출된 것이 보인다.
// 5. Continue (using the toolbar button or F9).
// 6. Debug tool window 에서는 현재 스코프에서의 모든 변수들이 보인다 :  $fruit, $fruits, $name, $person and the $_SERVER global.
//    $fruit 변수를 확장하여 무슨 값이 들어있나 확인한다. 세 값이 있는것을 볼 수 있다.
//    $person 변수는 확장하면 _name 변수가 있는 것이 보인다.
// 7. 표시되는 변수는 스코프 내의 변수들뿐이다. 이 변수들을 조사하기 위해 "pin" 을 꽂는다면 계속 검사가 가능하다.
//    context menu 에서 $name 변수를 추가하고 나타나는지 확인한다.
// 8. 이제 우리는 코드를 변경하지 않고 다른 이름으로 코드를 태스트 하려 한다.
//    Debugger Tool window에서 $person 을 확장하고 _name 프로퍼티를 F2 를 사용하여 당신의 이름으로 변경해 보자
// 9.  한번 Continue (F9) 를 하고 콘솔에 당신의 이름이 나오는 것을 확인한다
// 10. Debug Tool Window 에서 Evaluate Expression toolbar button (or Alt+F8) 를 사용해 이름을 변경한다.
//     즉, evaluate: $person->setName('Mikhail').
//     여기에서도 PHP코드를 실행할 수 있다. 예를들면 evaluate $a = 1 + 1. The Evaluate Expression Window 에는 그 결과가 나타난다.
