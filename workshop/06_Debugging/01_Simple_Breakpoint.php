<?php
/**
 * Simple Breakpoints
 *
 * 인터프리터에세 실행을 중단하고 변수를 추적할 수 있도록 일시 정지하는
 * 브레이크 포인트
 *
 * Ctrl+F8 (Windows/Linux)
 * Command+F8 (Mac OS X)
 */

namespace Debugging1\JetBrains;

$name = 'Maarten';
// 0. PhpStorm 은 "PHP 7.1 with XDebug" 로 XDebug가 활성화된 상태이어야 한다. Settings | PHP |CLI Interpreter 를 확인하자.\
// 1. 아래 코드에 브라이크포인트를 두자
$name = 'Mikhail';

for ($i = 0; $i < 5; $i++) {
    // 2. 아래 라인에도 브레이크 포인트를 두자
    $name = 'Person ' . $i;
}

// 4. 이 PHP 스크립트를 디버그하기 위해 context menu 를 사용하여 디버거를 실행하고 첫번째 브레이크 포인트에서 멈추게 한다.
//    현재 문장(실행하기 직전 상태)이 마크된 것이 보일 것이다.
//    하단의 tool window 에서 $name 변수는 'Maarten' 을 가지고 있을 것이다.
// 5. tool window 의 초록 아이콘이나 F9 (맥은 cmd+alt+R)  를 눌러 계속 실행한다.
// 6. 하단 tool window 는 $name 이 'Mikail' 임을 알려준다.
//     그릭 새로운 변수 $i 가 사용 가능하다. 0으로 설정하자.
// 7. 디버깅을 재개한다.
// 8. 매 루프를 돌 때마다 변수의 값이 수정되는 것이 추적될 것이다. been modified can be inspected.
