<?php
/**
 * Conditional Breakpoints
 *
 * 주어진 조건이 true 가 될 때만 인터프리터가 실행을 중단하고 변수를 추적한다.
 *
 * Ctrl+Shift+F8 (Windows/Linux)
 * Shift+Command+F8 (Mac OS X)
 */

namespace Debugging2\JetBrains;

$output = array();
for ($i = 0; $i < 5000; $i++) {
    // 1. 아래 코드에 브레이크포인트를 추가하고
    // 2. breakpoint 에 context menu를 사용하거나 단축키를 사용하여 $i == 4355 의 조건을 준다.
    $output[] = 'Person ' . $i;
}

// 3. Use the context menu to debug the current PHP script. This should launch the debugger.
// 3. PHP 스크립트를 디버그하기 위해 context menu 를 사용하여 디버거를 활성화한다.
// 4. 브레이크포인트는 $u 가 4355 일때만 활성화된다. 일반 브레이크 포인트라면 4000회 이상의 브레이크가 걸렸을 것이다.
//     Conditional breakpoint 는 특정 값에 대한 테스트를 진행하고자 할 때 유용하다.
