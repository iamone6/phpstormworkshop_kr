<?php
/**
 * Breakpoints
 *
 * 브레이크포인트에 추가 옵션을 설정한다.
 *
 * Ctrl+Shift+F8 (Windows/Linux)
 * Shift+Command+F8 (Mac OS X)
 */

namespace Debugging3\JetBrains;

$name = 'Maarten';
$fruits = array('apple', 'pear', 'lemon');
$currentFruit = '';

foreach ($fruits as $fruit) {
    // 1. Conditional breakpoint 를 추가하자 : $fruit == 'pear' 의 조건을 주자.
    echo $name . " likes " . $fruit . "\r\n";

    // 2. 아래 라인에 브레이크포인트를 추가한다.
    $currentFruit = $fruit;
}


// 3. 단축키를 사용해 모든 브페이크 포인트를 보자.
//    첫번째 브레이크포인트를 편집해 로그를 콘솔에 출력하도록 추가한다.
//    두번째 브레이크포인트를 편집해서 첫번째 브레이크포인트가 발동랑 때만 발동하도록 편집하자.
// 4. PHP script 를 디버그하기 위해 context menu 를 사용하자. 이는 디버거를 실행하게 할 것이다. 이제 콘솔은 다음과 유사한 문장을
//    출력한다 : Breackpoint reached : line 80 in 03_Breakpoints.php
// 5. 첫번째 브레이크 포인트가 실행되었기 때문에 스크립트 실행을 재개하면 두번째 브레이크 포인트에서도 멈추게 된다.
//  첫번째 컨디션에 해당하는 경우 디테일한 디버깅을 하려 할 때 아주 유용하다.

