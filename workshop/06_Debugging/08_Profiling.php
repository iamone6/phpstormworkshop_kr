<?php
/**
 * Profiling
 *
 * 호출횟수, 함수별 실행 시간 등에 대한 insight 를 제공받는다.
 * Note: Xdebug only
 */

namespace Debugging8\JetBrains;

require_once 'Code/FruitRepository.php';
require_once 'Code/Person.php';

use Debugging\JetBrains\FruitRepository;
use Debugging\JetBrains\Person;
//TODO: need to change for Docker
/*
1. 현재 파일에서 run configuretion 을 생성한다. context menu 를 사용하거나 configuration 을 실행한다.
2. 아래 라인을 Interpreter 옵션에 설정한다.
-d xdebug.profiler_enable=1 -d xdebug.profiler_output_dir=/var/www
3. 만들어진 configuration 을 실행한다.
4. 실행후, Tools | Analyze Xdebug Profiler Snapshot... 을 선택하고 project 디렉토리 내에 생성된 파일을 열자.
5. 이제 모든 함수간 서로 호출한 것, 시간, 퍼포먼스 이슈 등을 확인하고 분석할 수 있다.
*/





$fruitRepository = new FruitRepository();
$fruits = $fruitRepository->getAll();
/** @var Person[] $people */
$people = array();

for ($i = 0; $i < 200; $i++) {
    $people[] = new Person('Person ' . $i, $i);
}

foreach ($people as $person) {
    foreach ($fruits as $fruit) {
        // 2. 아래 코드에 브레이크포인트를 추가한다
        echo $person->getName() . ", age " .  $person->getAge() . ", likes " . $fruit . "\r\n";
    }
}
