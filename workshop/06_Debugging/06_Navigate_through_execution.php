<?php
/**
 * Navigate through execution.
 *
 * 실행 과정을 이동한다: Resume, Step Over, Step Into, Smart Step Into, Step out.
 *
 * F9 (resume), F8 (step over), F7 (step into) (Windows/Linux)
 * Alt+Command+R (resume), F8 (step over), F7 (step into) (Mac OS X)
 */

namespace Debugging6\JetBrains;

require_once 'Code/FruitRepository.php';
require_once 'Code/Person.php';

use Debugging\JetBrains\FruitRepository;
use Debugging\JetBrains\Person;

// 1. 아래 4개의 코드에 브레이크포인트를 추가한다.Place four breakpoints on the following four lines of code.
$fruitRepository = new FruitRepository();
$fruits = $fruitRepository->getAll();

/** @var Person[] $people */
$people = array();

for ($i = 0; $i < 200; $i++) {
    $people[] = new Person('Person ' . $i, $i);
}

foreach ($people as $person) {
    foreach ($fruits as $fruit) {
        // 2. Place a breakpoint on the following line of code.
        echo $person->getName() . ", age " .  $person->getAge() . ", likes " . $fruit . "\r\n";
    }
}

// 3. context menu 로 디버그를 시작하여 디버거가 첫번째 브레이크 포인트에서 멈추게 해라.
// 4. Resume을 사용하여 다음 브레이크포인트까지 가라. 키보드 단축키로 getAll() 함수 내부로 들어가라. 아재 우리는 FruitRepository 내부로
//    들어왔고 이 스코프에서의 모든 변수를 확인할 수 있다.
// 5. Step Out 으로 원래 자리로 돌아와라
// 6. Step Over로 다음 statement로 이동한다.우리는 statement단위로 움직이고 있다. Step Over 를 한번 더 해서 다음 statement로 이동한다.
// 7. Resume 으로 다음 브레이크포인트를 실행한다. 우리는 현재 루프 안에 있고 밖으로 나가고 싶다. 브레이크포인트를 해제하고 Resume 하라.
// 8. 이제 reading 구문인 echo $person->getName() . ", age " .  $person->getAge() . ", likes " . $fruit . "\r\n"; 위치에 와 있다.
//    여기에는 두개의 메소드 호출이 존재한다( getName() and getAge() ). Smart Step-Into 를 사용하여 getAge() 로는 들어가고 getName() 은 넘어가자.
//    IDE 에서 어떤 statement 로 들어갈 것인지를 물어오게 되면 적절히 대응하면 된다
