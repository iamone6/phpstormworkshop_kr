<?php
/**
 * Exception Breakpoints
 *
 * error, warning, notice, Exception 등이 발생했을 때 브레이크를 건다.
 * Note: XDebug 가 동작할 때만 가능하다.
 *
 * Ctrl+Shift+F8 (Windows/Linux)
 * Shift+Command+F8 (Mac OS X)
 */

namespace Debugging4\JetBrains;
use Exception;

// 1. 단축키로 모든 브레이크포인트를 본다.
// 2. 새 PHP Exception Breakpoint를 추가하고 Notice 에서 트리거 되도록 한다.
// 3. 새 PHP Exception Breakpoint를 추가하고 Warning 에서 트리거 되도록 한다.
// 4. 새 PHP Exception Breakpoint를 추가하고 Exception 에서 트리거 되도록 한다.
// 5. PHP 스트립트를 디버그하기 위해 context menu 를 사용한다. 디버거가 활성화 될 것이다.

// 6. breakpoint 가 없어도 0으로 나누려는 아래 문장은 Warning 이 트리거 되어 브레이크가 걸린다.
//    breakpoint에 마우스를 올려 어떤 에러인지 확인할 수 있다(warning)
//    Watches 패널에 errer_get_last() 함수를 추가하여 모니터링하자. 이 ㅎ롹장은 warning 메세지를 표시해 준다(division by zero)

$result = 100 / 0;

// 7. breakpoint 가 없어도 아래문장은 Exception 이기 떄문에 브레이크가 발동한다. 브레이크포인트에 마우스를 올려 에러의 종류를 확인(Exception) 하자.
throw new Exception('This is bad...');

// 8. Xdebug 를 사용할때 Exception 에서 breakpoint 를 사용하는 경험은 디버깅시에 error/Exception 에 대한 경계심을 일꺠워 줄 것이다.
