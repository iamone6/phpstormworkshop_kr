# SSH Terminal

원격 ssh 서버에 연결한다. IDE내에서 완전한 기능의 SSH 터미널을 제공한다.

0. "docker-compose up" 이 실행중임을 확인한다.

1. _Tools | Start SSH Session..._ 을 사용하여 "vm" server box에 연결한다.
    디렉토리를 `/home/jetbrains/upload` 로 이동하고 파일을 list해 본다. 여기는 PhpStorm 내 현재 프로젝트의 공유폴더이다. 
   
2. SFTP deployment 서버로 연결할 수 있다.  _Tools | Start SSH Session..._ 메뉴 커맨드를 사용하여 `vm` 서버를 선택한다.
3. _Edit credentials_ 옵션을 사용하면 host, port, username, password, SSH keypair 과 같은 연결 설정을 변경할 수 있다.
      얘룰 들면 아래 값을 사용할 수 있다:
      ```
      ip: localhost
      port: 2022
      user name: jetbrains
      password: jetbrains
      ```
      세부사항을 제공한 뒤엔, server 는 _Tools | Start SSH Session..._ 커맨드가 실행될 때 마다 보이게 될 것이다.

