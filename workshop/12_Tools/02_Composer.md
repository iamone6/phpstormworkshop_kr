# Composer

IDE에서 composer 의존성 관리자와 함께 작업하자. compposer 를 초기화하고, composer 의존성들을 검색하여 설치하자.
composer 프로젝트 타입을 사용하여 새로운 project 를 만들자. Command Line Tools 를 사용한다.

1. 현재 디렉토리에서 Composer 를 초기화한다.  _Project_ tool 윈도우에서 context menu 를 사용하여 _Composer | Init Composer_ 액션을 선택하자.
   IDE 가 `Composer.phar` 를 디스크의 폴더에 다운로드 받도록 한다.
2. `composer.json` 파일이 생성되고  _Add dependency_ 옵션이 Composer context menu 에 나타난다.
3. `phpoffice/phpexcel` 를 의존성으로 추가하고. `dev-master` version 을 설치한다.
4. `vendor` 폴더가 프로젝트에 추가되었고 Composre 는 `autoload.php`파일을 생성하였다.
5. (optional) 유닛테스트를 기억하는가? `autoload.php`  파일은 PHPUnit 이 composer 로 설치되었다면 PHPUnit 을 로딩할때도 사용할 수 있다.
   PHPUnit 을 Composer 로 설치하고 PHP 유닛테스트를 이 접근법으로 실행해 보자.