Remote SSH external tools

원격 서버로 수동 로그인 없이 원격지의 command 를 SSH를 통해 실행한다.
예를들어, 현재 캐시를 플러시하는 셸 스크립트를 실행하고 싶거나, 새 virtual machime 을 셋업하는 스크립트를 실행한다거나.

1. _Settings | Tools | Remote SSH External Tools_ 를 열고 새 tool 을 추가한다:
   * _Name_: `Hello World External Tool`.
   * _Show console when a message is printed to standard output stream_ 체크박스 선택.
   * _Program_: `echo`.
   * _Parameters_: `Hello World`.
   * _Connections settings_: `Current Vagrant`.
2. _Tools | External Tools | Hello World_ menu command를 선택하여 tool을 실행하자.
   tool 을 실행할때 타겟이 되는 서버를 선택할 수 있다.
3. 단축키를 tool에 할당하자. Alt+Shift+H 라면 괜찮을 것 같다.
   _Settings | Keymap_ 으로 가서 Hello World 를 검색한다.
   context menu 로 키보드 단축키로 지정한다.