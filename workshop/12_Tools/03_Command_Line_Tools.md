# Command Line Tools

command line tool 을 프레임워크와 라이브러리(Composer, Zend framework, Symfony)를 탑재한 채로 실행해 보자.
Symfony 콘솔 기반, Drush 등 으로 당신만의 작업을 진행하자. 자동완성도 지원한다. 하지만 이는 full console/terminal 이라는 의미는 아니다!ㅠ
(주: Drush 는 Deupal 용 커맨드 라인 shell. 즉, laravel 의 artisan 같은 것)

> * Ctrl+Shift+X (Windows/Linux)
> * Shift+Command+X (Mac OS X)


1. Command Line Tools을 Composer(나 다른 도구들)과 함께 사용할 수 있다.
   Command Line Tools 은 모든 프로젝트용으로 글로벌하게 혹은 프로젝트 단위로 추가될 수 있다. 우리는 Composer 를 현재 프로젝트에 추가하겠다.
   _Settings | Tools | Command Line Tool Support_ 에서 new tool 을 추가한다.
   Composer 를 tool로 추가한다. Zend Framework, Symfony, Symfony Console-based, Drush 등도 지원한다.
   다음 스템으로 `composer.phar` 가 있는 path 를 명시한다.
2. _OK_ 를 클릭 하면 tool 은 `c` alias 와 함께 추가되는데, 이는 Composer 를 `c`로 실행할 수 있다는 의미이다.
   단축키로 _Command Line Tools_ 를 열고 `c` 를 입력한다.
   자동완성이 지원된다. `requre` 커맨드를 선택하고 `psr/log:1.0.*@dev` 를 패키지로 명시한다:
    `c require "psr/log:1.0.*@dev"`

   이렇게 하면 `psr/log` 패키지가 프로젝트 루트에 설치된다.
3. _Custom Tool_을 추가해 보자. _Settings | Tools | Command Line Tool Support_ 에서 새 tool 을 추가한다.
    tool type 에서는 Custom Tool 을 선택한다. 현재 폴더의 `helloworld.sh` 나 `helloworld.bat` 로의 경로를 당신의 시스템에 맞게 명시한다.
   tool alias 로는 `hw` 를 사용한다.
4. _Command Line Tools_ 콘솔에서 새로 만든 _hw_ 툴을 실행한다 : `hw to Maarten`    
5. 커스텀 툴에 자동완성을 추가하려면, _Settings | Tools | Command Line Tool Support_ 으로 이동하고, 툴바의 
   _Open Definition in Editor_ 버튼을 클릭한다.
    이렇게 하면 자동완성 정보를 추가할 수 있는 XML 파일이 열린다. 이 파일 내용을 다음처럼 변경한다:

    ```
    <?xml version="1.0" encoding="UTF-8"?>
    <framework xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="schemas/frameworkDescriptionVersion1.1.3.xsd" name="Custom_hw" invoke="C:\Users\Maarten\Desktop\Workshop\11_Tools\helloworld.bat" alias="hw" enabled="true" version="2">
      <help><![CDATA[Hello World]]></help>
        <command>
            <name>to</name>
            <help>Say hello to someone.</help>
            <params>name</params>
        </command>
    </framework>
    ```
6. _Command Line Tools_ 콘솔에서 새로 만든 _hw_ 툴을 실행하고 자동완성이 동작하는지 확인한다.