# REST Client

빌트인 REST 클라이언트로 api API 들을 테스트할 수있다. 즉, 리퀘스트를 만들고 결과를 추적한다.

1. _Tools | HTTP Web Client | Test RESTful Web Service_ 메뉴를 선택하면 _REST Client_ tool 윈도우가 열린다.
2. 사용자 정보를 GitHub 에서 가져오는 리퀘스트를 만들자.
    * Specify the HTTP method to be `GET`
    * Set the host to `https://api.github.com`
    * Set the path to `/user`.
    
    헤더 섹션에는 `Accept: application/json` 헤더를 추가한다.
   추가로 request body 에 text 나 파일 업로드도 추가가 가능하다.
3. 리퀘스트를 실행한다. GitHub 은 인증이 필요하다는 응답을 보냈다.
4. 툴바의 _Generate an Authorization header_ 액션을 사용하여 Github cdedential(id/pw) 을 입력한다. IDE는 인증 헤더를 리퀘스트에 추가할 것이다.
5. 리퀘스트를 실행하고 GitHub에서 사용자 정보가 온 것을 확인하라.
6. _Response Headers_ 탭으로 이동하여 남은 rate limit 를 확인하라.(X-RateLimit-Limit: 5000, X-RateLimit-Remaining: 4999).
7. 리퀘스트는 툴바에서 계속 보낼 수 있다.
8. 리퀘스트는 툴바를 사용하여 import, export 가 가능하다. `samplerequest.xml` 를 import 하고 인증 헤더를 추가한 뒤 당신의 GitHub 리포지토리가 출력되는것을 확인하라.
(X-RateLimit-Limit: 5000,X-RateLimit-Remaining: 4998 )