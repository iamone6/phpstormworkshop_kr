# Local Terminal

PhpStorm 이 제공하는 로컬에서 동작하는 터미널.

1. _View | Tool Windows | Terminal_ 을 선택하여 로컬 터미널을 연다.
2. 시스템들을 돌아본다.
3. IDE는 멀티 터미널 세션을 지원한다. 툴바의 _+_ 버튼으로 새로운 터미널을 열 수 있다.
