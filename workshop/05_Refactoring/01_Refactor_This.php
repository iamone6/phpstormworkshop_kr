<?php
/**
 * Refactor This
 *
 * Ctrl+Shift+Alt+T (Windows/Linux)
 * Ctrl+T (Mac OS X)
 *
 * 프로젝트 뷰, Structure Tool 윈도우, 에디터, UML class 다이어그램 등에서 symbol 이나 code 등을 refactoring 한다.
 */

namespace Refactoring1\JetBrains;

// 1. 단축키를 사용하여 아래 변수명을 refactor 해 보자. 새 변수명은 $firstName 이다.
//  IDE에서 주석과 문자열도 검색할 수 있다는 점을 기억하자.
$name = 'Zaphod';

// 2. rename 리펙토링은 현재 스코프의 변수들 모두를 업데이트 시킨다.
echo $name . ' was here.';

// 3. `Refactor This menu`는 코드블럭을 대상으로도 동작한다. 아래 4줄의 코드를 선택하고 Refactor This 를 실행한다.
//    Extract 의  Method... 를 선택하고 이름을 "reverseString" 으로 한다. The $firstName 인자는 $stringToReverse 로 이름지을 수 있다.

    $nameBackwards = '';
    for ($i = 0; $i < strlen($name); $i++) {
        $nameBackwards = substr($name, $i, 1) . $nameBackwards;
    }


// 4. `Extract Method ` refactoring 이 아래의 코드는 리팰토링하지 않았다.
echo 'Name backwards: ' . $nameBackwards;

// 5. Project Tool 패널에서 현재 파일을 선택하자. Refactor This 를 여기서 사용하여 "01_Refactor_This_done.php" 로 변경하자.
//      Refactor 버튼을 사용하지 말고, 대신 Preview를 사용하여 어떤 파일들이 이 rename 으로 영향을 받을지를 먼저 살펴봐라.
//      Code.php 가 수정될 것이고, 이 코멘트 블럭의 exercise 03_Copy_Clone 의 파일명이 변경될 것이다. Do Refactor 를 클릭하여 리펙토링을
//      계속하자.