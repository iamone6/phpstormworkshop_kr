<?php
/**
 * Extract Field
 *
 * Ctrl+Alt+F (Windows/Linux)
 * Alt+Command+F (Mac OS X)
 *
 * expression 을 field(변수)로 추출한다.
 */

namespace Refactoring5\JetBrains;

class Greeter
{

    public function greet($name)
    {
        // 1. 문자열 'Hello' 를 $greeting 이란 필드로추출해서 수정이 가능하도록 하자.
        // 2. 마지막 편집내용을 Undo 하고, 새로운 필드를 constructor 에서 기본값으로 설장하도록 하자.
        //  HINT: "Class Constructor" 옵션을 사용하여 생성자를 만든다. (alt+Insert)
        return 'Hello' . ', ' . $name;
    }
}
