<?php
/**
 * Extract Variable
 *
 * Ctrl+Alt+V (Windows/Linux)
 * Alt+Command+V (Mac OS X)
 *
 * 선택한 표현식의 결과를 추출하여 변수에 넣는다. 원래 표현식은 새 변수값으로 변경된다.
 */

namespace Refactoring\JetBrains;

class Blog
{
    protected $blogName = 'My Blog';
    protected $blogSubtitle = 'Just another blog';

    public function renderHeader()
    {
        // 1. blogSubtitle 에 caret 을 위치시킨다.
        //    "{$this->blogName} - {$this->blogSubtitle}" 문자열을 추출하여 $title 이라는 변수에 넣는다.
        //    IDE는 어떤 표현식을 추출할 것인지를 물어올 것이다. 전체 문자열을 선택하자.
        echo '<title>';
        echo "{$this->blogName} - {$this->blogSubtitle}";
        echo '</title>';

        echo '<h1>';
        echo "{$this->blogName} - {$this->blogSubtitle}";
        echo '</h1>';
    }
}
