<?php
/**
 * Extract Method
 *
 * Ctrl+Alt+M (Windows/Linux)
 * Alt+Command+M (Mac OS X)
 *
 * 코드 블럭을 추출(Extract)하여 메소드화 한다.이때 변수도 포함된다.
 */

namespace Refactoring7\JetBrains;

// 1. 아래 코드의 마지막 3라인을 선택하여 Extract Method 를 실행한다.
//    Extract method refactoring 의 이름은 "reverseString" 으로 한다. $name 아규먼트는 $stringToReverse 로 한다.

$name = 'Eddie';
$nameBackwards = '';
for ($i = 0; $i < strlen($name); $i++) {
    $nameBackwards = substr($name, $i, 1) . $nameBackwards;
}

class Bootstrap
{
    public function run()
    {
        // 2. Select the following lines of code and extract them into a method called initialize(). Make it a public function.
        //    IDE는 $databaseName 과 $database 변수가 필요함을 감지하고 함수에서 리턴할 것이다.

        // Initialize settings
        $databaseName = 'sample';
        $database = mysqli_connect('localhost', 'root', '');

        // 3. 아래 코드들을 선택하여 createDatabase() 라는 protected method 로 추출해 집어넣자
        //    IDE는 $databaseName 과 $database 변수가 필요함을 감지하고 함수에서 리턴할 것이다.

        // Create database
        $query = "CREATE DATABASE $databaseName";
        mysqli_query($database, $query);

        // 4. 아래 코드들을 선택하여 sendEmail() 이라는 protected method 로 추출해 집어넣자
        //    IDE는 $databaseName 변수가 필요함을 감지하고 함수에서 리턴할 것이다.
        
        // Send out e-mail
        $body = 'The database ' . $databaseName . ' has been created!';
        $to = 'frankie.mouse@magrathea.org';
        mail($to, 'Database created', $body);
    }
}
