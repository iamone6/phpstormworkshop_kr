<?php
/**
 * Extract Constant
 *
 * Ctrl+Alt+C (Windows/Linux)
 * Alt+Command+C (Mac OS X)
 *
 * 상수를 추출하여 코드를 깔끔하게 정리한다.
 */

namespace Refactoring4\JetBrains;

// 1. "----------------------\r\n" 를 선택하고 alrl+alt+C로 상수 HORIZONTAL_LINE 으로 추출/ 변경하자.
echo "----------------------\r\n";
echo "Extracting Constants\r\n";
echo "----------------------\r\n";

// 2. 아래 클래스에서, 15 를 PAGESIZE 라는 상수로 추출하자.
//      IDE 는 상수명이 무엇이 될 지 예상하는데 꽤 익숙하다. (argument 변수명으로 만드는 듯)
class CustomerRepository
{
    public function getAll()
    {
        $db = new Db();
        return $db->getAll('SELECT * FROM customer', 15);
    }

    public function getAllActive(Db $db)
    {
        return $db->getAll('SELECT * FROM customer WHERE active = 1', 15);
    }
}

class Db
{
    public function getAll($query, $pageSize)
    {
        return array();
    }
}
