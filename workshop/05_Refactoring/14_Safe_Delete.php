<?php
/**
 * Safe Delete
 *
 * Alt+Delete (Windows/Linux)
 * Command+Delete (Mac OS X)
 *
 * 코드와 symbol을 안전하게 삭제한다
 */

namespace Refactoring14\JetBrains;

require_once 'Code/MoreCode.php';

// 1. Safe Delete the `14_Safe_Delete.php` file from the Project Pane.
//    다른 곳에서 사용중인 파일을 삭제하는 것은 안전하지 않음을 유의하자.
class CoolPerson
{
    protected $_name;

    public function __construct($name)
    {
        $this->_name = $name;
    }
}
