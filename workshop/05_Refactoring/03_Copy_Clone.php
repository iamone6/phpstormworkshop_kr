<?php
/**
 * Copy/Clone
 *
 * F5 (copy) (Windows/Linux/Mac OS X)
 *
 *
 * 클래스, 파일, 디렉토리 등을 카피하여 다른 디렉토리로 복사하거나 동일 디렉토리에 사본을 만듦.
 */

namespace Refactoring3\JetBrains;

// 1. 01_Refactor_This.php file 을 Code directory 로 카피하자.
//      Project Tool Window 에서 작업하고 마우스를 사용하지 말자.
//    HINT: Alt+1/Command+1, arrow keys, F5

// 2. 현재 파일을 복제하여 "03_Copy_Clone_(cloned).php" 라고 이름짓자.
//    Use the F5 keyboard shortcut from within the editor.

// 3. 현재 파일을 Code directory에 복사하자. Ctrl 키를 누른채로 Drag/drop 을 하면 된다.
