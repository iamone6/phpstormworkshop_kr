<?php
/**
 * Change Signature
 *
 * Ctrl+F6 (Windows/Linux)
 * Command+F6 (Mac OS X)
 *
 * 이 기능은 다음을 리펠토링 하는데 사용된다:
 *  - 함수명 변경
 *  - 새 파라메터를 추가하거나 존재하는 파라메타를 제거
 *  - 파라메타에 기본값 할당
 *  - 파라메터 재정렬
 *  - 파라메타명 수정
 *  - 함수의 호출 hierarchy 를 통한 새로운 파라매터를 전파
 */

namespace Refactoring2\JetBrains;

// 1. `Change Signature` refactoring 으로 함수명을 "add"로 변경하자
function addNumbers($a, $b)
{
    return $a + $b;
}

// 2. 1번에 의해 여기(호출하는 부분)도 영향을 받았다.
$four = addNumbers(2, 2);

// 3. sayHello의 signature 를 변경해 보자: $name 파라메터를 $firstName 으로 변경하고 $lastName 파라메터를 추가한다.
//      $lastName 에 기본값을 추가적으로 명시한다.
//      e.g. '' 를 사용하여 sayHello 가 문제없이 호출되도록 한다.
//
//      여기에, 새로운 파라메터를 전파할 수 있다. 아래의 usingSayHello() 도 $name 을 사용하고 있고, 우리는 새 $lastName 변수를
//      전달하고자 한다(함수 호출시 변수명과 사용된 파라메터명을 업데이트)
//      refactor 윈도우에서, `Propagate Paramaters` 아이콘을 누른다.
//
//      이리보기로 확인후 문제가 없으면 적용하자.
function sayHello($name)
{
    // 4. 아래 라인의 주석을 해제하자.
    // $name = $firstName . ' ' . $lastName;
    return "Hello, $name";
}

sayHello('Marvin');


class UsingSayHelloClass
{
    // 5. 리펙토링 후 `Change Signature` 를 통해 $firstName 과 $lastName 파라메터를 재정렬한다.
    // 6. Signature 를 변경하고 $firstName의 기본값을 '' 등으로 변경한다.
    //      중료 : 기본값 컬럼에 기재하면 안된다. 이 컬럼에 기재하면 이 함수를 호출하는 모든 곳에서 변경한 값으로 바뀌어 버린다.
    //              그 대신 $firstName 을 $firstName = '' 으로 변경해야 한다.
    //
    //      문제가 없으면 refactoring 을 진행한다.
    public static function usingSayHello($name)
    {
        sayHello($name);
    }
}

UsingSayHelloClass::usingSayHello('Marvin');
