/**
 * Extract Parameter
 *
 * Ctrl+Alt+P (Windows/Linux)
 * Alt+Command+P (Mac OS X)
 *
 * 새 파라메터를 함수 정의부에 추가한다. default 값을 고려해야 하며, JSDoc 도 만들어 낸다.
 */

(function (namespace) {
    namespace.person = function (name) {
        var _name = name;

        this.greet = function () {
            // 1. 'Hello' 문자열을 "greeting" 이라는 파라메터로 만들자. ('Hello'를 선택하고 ctrl+alt+P)
            //    어떤 부모 함수로부터 넘어올 것인지를 선택할 수 있다. (IDE가 물어본다)
            //    greet() 함수에 이것을 추가하자.
            //    JSDoc 주석을 생성하자.
            //    옵션으로 IDE는 'Hello' 를 기본값으로 제공하게 될 것이다.
            return 'Hello' + ', ' + _name;
        }
    }
})(window.jetbrains = window.jetbrains || {});
