<?php
/**
 * Move Static Member
 *
 * F6 (Windows/Linux/Mac OS X)
 *
 * static fields 와 methods 를 다른 타입으로 변경한다
 */

namespace Refactoring15\JetBrains;

// 1. Utils 클래스의 static method "split" 을 StringUtils 로 옮기자.
class Utils
{
    public static function split($subject, $delimiter)
    {
        return explode($delimiter, $subject);
    }
}

class StringUtils
{
}

// 2. Utils::split() 가 StringUtils::split() 로 수정되었음을 확인하자.
$myArray = Utils::split('This is a test', ' ');
$myCsvRecord = Utils::split('Maarten;JetBrains', ';');
