<?php
/**
 * Rename
 *
 * Shift+F6 (Windows/Linux/Mac OS X)
 *
 * symbol( 변수명, 함수명 등) 의 이름을 변경한다. 자동으로 코드 내에서 이 이름을 참조하는 부분도 함께 변경된다.
 */

namespace Refactoring13\JetBrains;

// 1. `Rename` 기능으로 Person 을 Customer 로 클래스명을 변경하고 오버타이핑하자. 프로젝트의 모든 사용처에서 이름이 함께 변경된다.
// 2. 마지막 rename 을 Undo 하고 Person 클래스를 다시 Rename refactor 로 변경하자
//
//    오버타이핑시에 Shift+F6 를 다시 사용하여 advanced 다이얼로그를 활성화하고 주석과 문자열에서 검색을 활성화하자.
//    옵션을 선택하여 rename 을 실행하자 - 미리보기에서 주석이 업데이트 되는 것을 확인하자.
class Person
{
    // ...

    // 3. sayHello() 를 sayGoodbye()로 변경한다.
    public function sayHello()
    {
        return 'Hello!';
    }

    // 4. $name parameter 를 $string 으로 변경한다. PHPDoc block 도 함께 변경된다.
    /**
     * @param string $name Name of the person.
     */
    public function greet($name)
    {

    }
}

$person = new Person(); // creates a new Person
$person->sayHello();

// 5. 현재 파일명을 "13_Rename_done.php" 로 변경한다. 파일의 모든 용례가 업데이트될 것이다.
