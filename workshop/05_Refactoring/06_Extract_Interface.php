<?php
/**
 * Extract Interface
 *
 * 클래스로부터 인터페이스를 추출한다.
 */

namespace Refactoring6\JetBrains;

// 1. Refactor This 를 사용(See excercise 01_Refactor_This)하여 CustomerRepository 로부터 Interface 를 추출(Extract)하라.
//    이 인터페이스를 IRepository 로 하고, JetBrains\Contracts 로 네임스페이스를 정하라.
//    PHPDoc block 을 복사하여 get(), getAll(), save(), delete() 메소드가 추출(Extract)되도록 하라.
//    "Replace Class references with interface where possible"  를 체크하면 CustomerController 클래스를 업데이트할 것이다.
//
//    refactoring 을 적용하라.

class CustomerController
{
    // 2. PHPDoc 블럭이 수정되었음을 확인하고 IRepository 가 거기 있음을 확인하라.
    /** @var CustomerRepository */
    protected $_repository;

    function __construct()
    {
        $this->_repository = new CustomerRepository();
    }
}

class CustomerRepository
{
    /** @var Db */
    protected $_db;

    function __construct()
    {
        $this->_db = new Db();
    }

    /**
     * Get by id.
     *
     * @param int $id ID
     * @return mixed Instance.
     */
    public function get($id)
    {
        return $this->_db->get($id);
    }

    public function getAll()
    {
        return $this->_db->getAll('SELECT * FROM customer', 15);
    }

    public function save($instance)
    {
        $this->_db->save($instance);
    }

    public function delete($id)
    {
        $this->_db->delete($id);
    }
}

class Db
{
    public function getAll($query, $pageSize)
    {
        return array();
    }

    public function save($instance)
    {
    }

    public function delete($id)
    {
    }
}
