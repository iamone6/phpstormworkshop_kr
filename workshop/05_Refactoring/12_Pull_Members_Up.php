<?php
/**
 * Pull Members Up / Push Members Down
 *
 * 멤버변수/함수들을 자식에서 부모로, 부모에서 자식 클래스로 이동시킨다.
 */

namespace Refactoring12\JetBrains;

// 1. getName() 을 SilverCustomer 에서 Person 으로 가져오라. `Refactor This` (ctrl+shift+alt+T)를 사용하라.
//      역주 : 이때 target Class 에서 $name 을 참조할 수 없다는 경고가 나오는데, IDE 버그로 보인다.
// 2. calculateDiscount() 를 Customer 로 내려보내라. `Refactor This` 를 사용하라.
//      calculateDiscount() 는 Person 클래스에서 호드를 호출하는 것으로 되어 있기 때문에
//      이 이동으로 인해 문제가 감지될 것이다.
class Person
{
    protected $name;

    public function calculateDiscount($amount)
    {

    }
}

class Customer extends Person
{

}

class SilverCustomer extends Customer
{
    public function getName()
    {
        return $this->name;
    }
}

$customer = new SilverCustomer();
$customer->getName();

$person = new Person();
$person->getName();
$person->calculateDiscount(100);
