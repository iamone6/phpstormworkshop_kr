<?php
/**
 * Move
 *
 * F6 (Windows/Linux/Mac OS X)
 *
 * 파일, 디렉토리, 클래스 및 스태틱 멤버의 위치를 변경한다.
 */

namespace Refactoring11\JetBrains;

// 1. 현재 이 파일을 Code 디렉토리로 이동하라
// 2. Code 디렉토리 밑을 MoreCode 디렉토리로 이동하라.
// 3. Class1, Class2, Class3 를 각각 분리된 파일로 이동시켜라

class Class1
{
}

class Class2
{
}

class Class3
{
}

// 4. Move the static method log() from the Utilities class to the Logger class.
class Utilities
{
    public static function log($message)
    {
        echo $message;
    }
}

class Logger
{

}

// 5. Note that the calls to Utilities::log() have been updated.
Utilities::log('Almost at the end of the exercise');
Utilities::log('At the end of the exercise');
