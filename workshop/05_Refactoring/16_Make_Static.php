<?php
/**
 * Make Static
 *
 * 메소드를 static 메소드로 변경한다.
 *  - 이것은 어떻게 해야 할지 몰라서 테스트를 못함.
 */

namespace Refactoring16\JetBrains;

// 1. split 메소드를 static으로 변경하자.
class Splitter
{
	private $delimiter;

	public function __construct($delimiter)
	{
		$this->delimiter = $delimiter;
	}

	public function split($subject)
	{
		return explode($this->delimiter, $subject);
	}
}

// 2. 인스턴스에 제공된 아규먼트를 그대로 유지하면서
// Splitter::split() 으로 static method 호출로 모두 변경되었다.
$splitter = new Splitter(' ');
$myArray = $splitter->split('This is a test');
$myCsvRecord = $splitter->split('PhpStorm JetBrains');
