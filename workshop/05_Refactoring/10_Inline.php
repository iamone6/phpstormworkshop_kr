<?php
/**
 * Inline
 *
 * Ctrl+Alt+N (Windows/Linux)
 * Alt+Command+N (Mac OS X)
 *
 * 중복되는 변수나 함수를 하나의 식으로 변경하는 기능. Extract Method와 반대되는 기능이다.
 */

namespace Refactoring\JetBrains;

// 1. $name에 caret 을 두고 Inline을 실행하여 $name 과 Echo 를 합쳐버리자.
$name = 'Benji';
echo $name;
