Connecting to a database (server)

MySQL, DB2, Derby, SQL Server, Oracle, PostgreSQL, Sybase, H2, sqlite, Google 등등에 연결한다.

0. "docker-compose up" 이 실행 상태임을 확인한다.
1. Database tool window 를 연다 (use Search Everywhere). or  IDE 오른쪽에 숏컷탭이 보인다.
2. Database tool window toobar에서 , Data Source Properties 를 열자.
3. MySQL driver file을 다운받는다.
4. Use password: jetbrains
5. 커넥션을 저장 후, Database tool window 에서 노드를 열고 jetbrains database 에 접근할 수 있는지 확인한다.