# Table Editor

테이블 내의 레코드를 편집한다 : insert, update, delete, ...

> F4 (Windows/Linux/Mac OS X)

1. _Database_ tool window 에서  _person_ table 을 선택하고  _Table Editor_ 를 실행한다.
2. 테이블 내의 모든 레코드가 보인다. 셀을 더블클릭하면 편집 가능 상태로 변경된다.
3. _Submit changes immediately_ 옵션이 비활성화일 때에는 변경사항이 자동으로 적용되지 않는다.
   이 옵션을 비활성화하고 row 를 추가하거나 변경해 보자.
   그 후 버튼을 사용하여 커밋이나 롤백을 수행한다.
4. Row 에 필터링이 가능하다. _Filter_ toolbar button을 클릭하고 다음 필터를 만들어 보자: `city_id > 2`.
   이때 자동완성이 동작하는 것도 확인하자.
   _View Query_ button 을 누르면 쿼리가 생성된 것도 확인이 가능하다.
5.  `city_id` 컬럼의 값을 선택한 후 Ctrl+Q (F1)를 눌러서 다흔 테이블에서 이 값이 어떻게 참조되는지도 확인하자.
5. Rows 는 export된다. 툴버 버튼을 사용하여 `INSERT` 구문으로 rows 를 export하자.