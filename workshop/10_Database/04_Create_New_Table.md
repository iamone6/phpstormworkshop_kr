# Create New Table

데이터베이스에 새 테이블을 생성한다. 컬럼 옵션, 기본값, promary key.. 등등을 명시한다.

> * Alt+Insert (Windows/Linux)
> * Command+N (Mac OS X)

1. 새 테이블을 만들고 `attendee`라 명명한다. 아래처럼 테이블을 만들려면 다이얼로그를 사용한다

    ```
    CREATE TABLE workshop.attendee
    (
      course_name VARCHAR(255) NOT NULL,
      person_id int NOT NULL,
      comments MEDIUMTEXT,
      PRIMARY KEY ( course_name, person_id )
    );
    ```

2. _Database_ tool window에서, `attendee.person_id` 칼럼을 찾아 foreign key 를 `person.person_id` 라는 이름으로 생성한다.
   테이블 명은 `person` 으로 입력한다. IDE는 `person__id` 가 foreign ley 로 사용되야 하는 컬럼으로 판단하게 될 것이다.
3. `workshop_database.sql` 파일을 현재 테이블과 함께 업데이트한다. 
    이는 workshop database 를 선택하고 _Generate and Copy DDL_ 액션(ctrl+shift+C/cmd+shift+C) 을 사용하여 작업이 가능하다.
   