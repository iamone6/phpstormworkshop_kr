# Database Console

자동완성, 검사, 에러 헨들링, 조인문 자동완성 등등..을 지원하는 SQL 콘솔

> * Ctrl+Shift+F10 (Windows/Linux)
> * Shift+Command+F10 (Mac OS X)

1. (01_connect_to_a_database.md 에서 만든) jetbrains database 연결용 database 콘솔을 열고 workshop_database.sql 파일의 내용을 복사해 넣는다.
2. SQL 구문 하나를 선택하여 실행할수도, 여러 구문을 선택하여 실행할 수도 있다. 콜솔의 모든 구문을 선택하고 실행하려면 
  툴바의 버튼을 이용하던가, ctrl+enter(cmd+enter) 를 사용하라.
3. 새 콘솔을 열고 아래 쿼리를 입력하라. `person`입력시에 자동완성이 동작함도 확인하라.

    `SELECT person.name FROM person`

4. 쿼리를 실행하고 그 결과를 지켜본다.
5. 다음 쿼리를 입력하자:

    `SELECT person.name FROM people`

6. 코드에서 알수 있듯이, 콘솔은 _people_ 테이블이 존재하지 않음을 검사하여 알려준다.
7. 다음 쿼리를 입력하자:

    ```
    SELECT person.name, city.name as city, country.name as nationality
    FROM person
      INNER JOIN city ON person.city_id = city.city_id
      INNER JOIN country ON
    ```

8. 이제 스마트 자동완성(CTRL+SHIFT+SPACE) 를 사용하여 IDE 가 완전한 `JOIN` 문장을 추천하는 부분을 살펴보자.
   아래 쿼리를 완성하자:

    ```
    SELECT person.name, city.name as city, country.name as nationality
    FROM person
      INNER JOIN city ON person.city_id = city.city_id
      INNER JOIN country ON country.country_id = person.nationality_country_id
    WHERE country.name = ?
    ```

9. 구문을 실행해보자. IDE는 `?` (이것은 `:country` 아니면 이와 비슷한 구문으로 대체될 수 있다 ) 에 무엇이 들어가야 할 지를 물어온다.
    `'Belgium'` (작은따옴표 포함) 으로 값을 입력해 보자. 
 
10. results tool window에서 그 결과를 살펴보자.
