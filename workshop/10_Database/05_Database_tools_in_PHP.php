<?php
/**
 * Database tools in PHP
 */

// 1. 새 변수 $query 에 "SELECT * FROM person" 라는 쿼리를 담자.
//    query 를 직접 입력하고 자동 완성이 되는 것을 확인한다(FROM 뒤에서 ctrl+space 를 누름)

$query = "";

// 2. 쿼리를 좀 더 완성하자. JOIN 구문의 스마트 완성구문을 기억하는가?  ON 뒤에서 Ctrl+Shift+Space 를 눌러보자.

$query = "SELECT person.name, city.name as city, country.name as nationality
            FROM person
              INNER JOIN city ON person.city_id = city.city_id
              INNER JOIN country ON ";

// 3. 위 퀴리 내에서 Alt+Enter 를 눌러 콘솔에서 실행시켜보자.
