# VCS 명령

VCS로 파일을 추가, 삭제, 변경점 커밋을 수행한다.

> * Alt+Backquote (Alt+`) (Windows/Linux)
> * Ctrl+V (Mac OS X)

1. VCS operations popup 을 열고 현재 파일을 현재 변경점(changeset)에 추가하자.
    Project Tool Window 에서 파일의 색이 변경되는 것을 확인하자.
2. Git 에서 이는 파일이 VS 에 들어갔다는 것을 의미하지는 않는다: chagneget 에만 들어갔을 뿐이다.
   VCS Operations popup을 사용하여 프로젝트의 나머지 파일들도 동일하게 추가하라.
   어떤 폴더에서도 이 팝업을 사용할 수 있다는 점을 기억하자.
3. 이제 모든 파일들이 초록색이 되었을 것이고, 이것은 현재 changeset 에 추가되었음을 의미한다.