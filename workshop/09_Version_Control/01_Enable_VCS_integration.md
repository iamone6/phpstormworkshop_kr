# VCS 와의 통합

IDE에 이 프로젝트가 버전 콘트롤 시스템 산하에 있음을 알려줘라

1. _VCS_ menu 를 확인한다.

   Git, Mercurial, Subversion, ... 등의 VCS로부터 코드를 체크아웃 받을 수 있다.
   이 프로젝트를 VCS 시스템에 임포트 하거나 GitHuv 에 공유할 수있다.
   우리는 VCS 통합을 할 것이다. 
   
2. _VCS | Enable VCS Integration_ 메뉴 커맨드를 열고 이 프로젝트가 Git 저장소를 사용한다는 것을 알려주면, 프로젝트내 폴더에
_.git_ 폴더가 생성된다. _Project_ tool window 에서 모든 파일들의 색상이 변경된 것을 확인하자. 이는 해당 파일이 아직 소스 콘트롤 상태가 아니라는 의미이다.