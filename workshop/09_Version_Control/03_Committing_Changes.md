# 변경을 커밋하기

변경된 것들을 VCS에서 주석, 작성자 등을 포함시켜 논리적 작업으로 유지시킨다.

> Ctrl+K (Windows/Linux)
>
> Command+K (Mac OS X)

1. Commit Changes 로 현재 changeset 을 Git 저장소에 커밋한다.
2. 윗 영역의 리스트는 변경되고 추가되고 삭제된 파일을 보여준다.
   커밋 메세지로 논리적인 변화가 무엇이 있었는지를 묘사하여 기록한다.
   지금 케이스라면 "Initial commit" 이 될 것이나, 일반적으로는 코드의 변경점인 버그나, 이슈 넘버, 짧은 설명들이 들어갈 것이다.
   이때 IDE가 자동으로 코드를 reformat하거나, import 를 최족화 시키거나, 코드 내의 TODO 커멘트에 대해 경고를 줄 수도 있다.
   이런 것들의 커밋이 완료되면 이 변경점을 원격 서버에 바로 업로드가 가능하게 된다.
3. 변경된 내용을 커밋하자.
4. Project tool window 에서 이제 파일명은 자연스러운 색상으로 변경된다. 이 말은 VCS에 존재하는 버전과 동일하다는 의미이다.
5. 현재 파일에 수정을 가해보자.(enter your name here: ""). 에디터는 왼쪽 gutter 에 푸른색 색상 표시가 나타나는데, 마지막 VCS 커밋 이후로
   해당 라인에 변화가 있다는 의미이다.
   Project tool window에서도 변화가 있는 파일들은 색상이 변한다.
6. 이 텍스트의 아래에 라인을 추가해 보자. 라인이 추가되면 left gutter 에 푸른색이 표시된다.
7. 다시한번 모든 수정내용을 커밋하고 커밋에 설명을 추가하자.
