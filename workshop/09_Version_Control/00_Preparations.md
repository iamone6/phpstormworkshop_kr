# Version Control System (VCS) - 준비사항

이 연습은 몇가지 사전 준비가 필요하다.
이 워크샵 프로젝트가 GitHub 에서 왔기 때문에, 설정되어있는 VCS세팅을 PhpStrom에서 삭제하여 처음부터 시작할 수 있도록 한다.

1. 현재 프로젝트 루트 폴더를 PhpStrom 외부에서 열고 _.git_ 폴더를 삭제한다. 
2. In the Settings | Version Control, make sure that all VCS bindings are removed.
2. Settings | Version Control 에서 모든 VCS 바인딩이 삭제된 것을 확인한다.
3. Settings | Version Control | Git 에서 실행 가능한 Git 경로가 있는 것을 확인한다.
 
 이제 프로젝트는 VCS와의 연결고리가 없어져서 준비가 끝났다.