<?php
/**
 * Local history
 *
 * Undo 를 실행한다. 커밋들 간의 변경된 점을 화면에 표시한다, 로컬에서 변경된 것들을 되돌린다, ...
 */

namespace VersionControl\JetBrains;

// 1. 아래 변수의 값을 당신의 이름으로 변경하고 파일을 저장한다.
$name = 'Maarten';

// 2. $name 변수값의 첫 글자를 대문자로 만든다
//    Code: $name = ucfirst($name);
//    파일을 다시 저장한다.

// 3. VCS Operations popup(alt+`)에서 Local History 하위의 Show History 를 선택한다.
//    현재 파일에 대해 로컬에서 변경된 점을 알려주는 새창이 뜰 것이다.
//    여기서 우리는 코드 한줄이 수정되고 한줄이 추가되었음을 확인할 수 있다.
// 4. 마지막으로 수정한 변경을 되돌란다.
// 5. 현재 파일의 맨 첫 버전으로 롤백한다.
// 6. 현 프로젝트의 폴더에서 Local History를 검사한다. (프로젝트 내 폴더 -> 우클릭 -> Local History)
//    VCS 커밋간의 내용을 포함하여 우리가 진행한 모든 변경된 것들을 확인할 수 있다.