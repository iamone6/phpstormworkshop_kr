<?php
/**
 * Filtering
 *
 * 패턴을 기반으로 하여 tool window 에서 필터 작업을 수행한다.
 *
 * Alt+6 (Windows/Linux)
 * Command+6 (Mac OS X)
 */

namespace Todo3\JetBrains;

// 1. TODO tool window 를 열고(alt+6) Project scope로 선택되었는지 확인한다(현재 프로젝트의 모든 TODO 가 보이도록)
// 2. TODO window의  filter 아이콘(깔때기 모양) -> Edit filters 아이콘을 클릭하여 두개의 새 필터를 만든다:
//     - Verify (Maarten), selecting the \bverify\[\bMaarten\b\].* pattern.
//     - Review, selecting the \breview\b.* pattern.
// 3. TODO tool window 에서 Review comments 만 보도록 필터를 설정한다.
// 4. TODO tool window 에서 Maarten에 대한 comment 만 보이도록 필터를 설정한다.
// 5. 팀 내에서 username 으로 패턴을 설정하면 진짜 내가 할 일만 볼수 있게 할수 있어 편리한다.
