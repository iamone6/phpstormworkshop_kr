<?php
/**
 * Patterns
 *
 * 다른 키워드를 사용하더라도 todo 로 인식할수 있도록 todo pattern을 커스터마이즈한다.
 *
 * Ctrl+Alt+S (Windows/Linux)
 * Command+, (Mac OS X)
 */

namespace Todo2\JetBrains;

// 1. Settings Pane -> Editor | Todo 로 이동.
//    다음과 같이 새 패턴을 추가하자: \breview\b.*
//    그러면 아래의 주석이 todo로 하이라이트 된다.

// review: check if this line is highlighted and recognized by PhpStorm's todo system.

// 2. 코드 리뷰에서 Maarten 이 작업해야할 항목들을 명시하는 패턴을 추가하자: \bverify\[\bMaarten\b\].*
//    아래의 코멘트가 하이라이트 될 것이다.

// verify[Maarten]: check if this line is highlighted and recognized by PhpStorm's todo system.

// 3. Open the TODO Tool Window and see if these new patterns are shown in there.