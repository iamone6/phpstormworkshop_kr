<?php
/**
 * Todo
 *
 * Alt+6 (Windows/Linux)
 * Command+6 (Mac OS X)
 */

namespace Todo1\JetBrains;

// 1. 단축키로 프로젝트내의 모든 todo 를 확인한다.
// 2. 그 범위를 좁혀 지금 파일의 todo 만 보이도록 스코프를 변경한다.
// 3. 더블클릭이나 단축키 F4로 맨 처음 todo 주석으로 이동한다.
// 4. index.html 의 TODO 주석을 찾는다. TODO는 IDE 에서 모든 종류의 언어들에 대해 동작한다.
// 5. fixme 나 todo 를 주석에 적어서 새로운 todo 아이템을 추가한다. 예를들면 :
// todo: create a new comment below this line
