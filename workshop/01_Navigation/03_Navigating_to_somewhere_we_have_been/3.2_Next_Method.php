<?php
/**
 * Next Method
 *
 * Alt+Down / Alt+Up (Windows/Linux)
 * Ctrl+Down / Ctrl+Up (Mac OS X)
 *
 * 파일/클래스 내에서 메소드 간 이동을 합니다.
 *
 * NOTE : OSX Mountain Lion 이후 최신 버전에서는 Ctrl+Down / Ctrl+Up 이 시스템 단축키에 의해 overwrite 됩니다.
 * 그래서 이 단축키를 사용하기 위해서는 이 키들을 다시 bind 시켜줘야 합니다.
 */

namespace Navigation3\JetBrains;

class NextMethod
{
    // 1. Next Method 기능을 사용해서 thirdFunction() 으로 이동합니다.
    public function firstFunction()
    {
    }

    public function secondFunction()
    {
    }

    // 2. Previous Method 를 사용하여 firstFunction() 로 이동합니다.
    public function thirdFunction()
    {
    }

    public function fourthFunction()
    {
    }
}
