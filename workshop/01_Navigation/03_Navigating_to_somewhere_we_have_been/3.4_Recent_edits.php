<?php
/**
 * Navigate to Last Edit Location.
 *
 * Ctrl+Shift+Backspace (Windows/Linux)
 * Shift+Command+Delete (Mac OS X)
 *
 * 코드가 마지막으로 편집된 위치로 이동합니다.
 */

namespace Navigation3\JetBrains;

class NavigateRecent
{
    public function startingPoint()
    {
        // 1. step1() 으로 이동합니다.
        $this->step1();
    }

    public function step1()
    {
        // 2. 아래의 주석을 해제합니다.
//        echo 'In step 1.';

        // 3. step2()로 이동합니다.
        $this->step2();
    }

    public function step2()
    {
        // 4. Last Edit Location 을 사용하여 마지막으로 편집한 위치로 이동합니다.
    }
}