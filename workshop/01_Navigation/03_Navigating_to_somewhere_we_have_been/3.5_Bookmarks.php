<?php
/**
 * Bookmarks
 *
 * Windows/Linux:
 *   F11 북마크를 토글합니다.
 *   Ctrl+F11 넘버링이 된 북마크(Bookmark with Mnemonic)를 토글합니다.
 *   Shift+F11 북마크를 표시합니다.
 *   Ctrl+0..9 넘버링이 된 북마크로 이동합니다.
 *
 * Mac OS X:
 *   F3 Toggle Bookmark
 *   Alt+F3 Toggle Numbered Bookmark (Bookmark with Mnemonic)
 *   Command+F3 Show bookmarks
 *   Ctrl+0..9 Navigate to numbered bookmark
 *
 * 북마크를 설정 및 해제하고 해당 북마크로 이동합니다.
 *
 *
 * 1. firstFunction() 으로 이동하고 (use Navigate to Symbol or Search Everywhere), 북마크를 설정하세요.
 * 2. secondFunction() 으로 이동하고 북마크를 설정하세요.
 * 3. thirdFunction() 으로 이동하고 넘버링된 북마크를 설정하세요, 북마크 3번에 할당하세요.
 * 4. 북마크 리스트를 표시시키고 북마크들간에 이동시켜 보세요.
 * 5. 넘버링된 북마크로 이동하세요. (Ctrl+3).
 * 6. secondFunction()에서 북마크를 제거하세요.
 * 7. 북마크 리스트를 열고, 북마크 중 하나에 description 을 추가해 보세요.(좌상단의 연필 아이콘)
 */

namespace Navigation3\JetBrains;

class Bookmarks
{
    public function firstFunction()
    {
    }

    public function secondFunction()
    {
    }

    public function thirdFunction()
    {
    }
}
