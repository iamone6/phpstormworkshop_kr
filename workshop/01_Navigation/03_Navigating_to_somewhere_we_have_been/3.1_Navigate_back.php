<?php
/**
 * Navigate back and forward
 *
 * Ctrl+Alt+Left / Ctrl+Alt+Right (Windows/Linux)
 * Command+[ / Command+] (Mac OS X)
 *
 * 이동 직전의 위치로 되돌아 가거나 다시 이동합니다.
 */

namespace Navigation3\JetBrains;

class NavigateBack
{
    public function startingPoint()
    {
        $navigateTo = new NavigateTo();

        // 1. destination() 로 이동합니다.
        // 3. 단축키를 사용하여 앞으로 이동합니다.(ctrl+alt+Right)
        $navigateTo->destination();
    }
}

class NavigateTo
{
    public function destination()
    {
        // 2. 단축키를 사용하여 이전으로 되돌아갑니다 (ctrl+alt+Left)
    }
}
