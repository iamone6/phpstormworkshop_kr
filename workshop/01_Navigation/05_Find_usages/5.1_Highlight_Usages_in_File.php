<?php
/**
 * Highlight Usages in File
 *
 * Ctrl+Shift+F7 (Windows/Linux)
 * Shift+Command+F7 (Mac OS X)
 *
 * 현재 파일 내에서 선택된 symbol 이 사용된 곳에 하이라이트를 표시한다.
 */

namespace Navigation5\JetBrains;

class HighlightUsagesInFile
{
    const MIN_VALUE = -3.5;
    const MAX_VALUE = 3.5;

    // 1. $newValue 에 caret 을 위치시켜보면 자동으로 해당 영역이 사용된 곳에 언더라인이 표시된다.
    //      읽기로 사용될때와 쓰기로 사용될때 하이라이트 컬러가 서로 다름에 주목하자.
    //
    //  2. caret 을 $newValue에 놓고 Highlight Usages in File 을 실행한다.
    //     read/ write 상황에 따라 하이라이트 컬러가 다른것을 주목하자.
    //     에디터의 오른쪽에 가로선이 생겨서 하이라이트된 영역이 표시되고 있음도 확인하자.
    //     ESC를 누르면 하이라이트는 사라진다.
    //
    public function adjust($original, $delta)
    {
        $newValue = $original + $delta;

        if ($newValue < self::MIN_VALUE) {
            $newValue = self::MAX_VALUE;
        }
        if ($newValue > self::MIN_VALUE) {
            $newValue = self::MAX_VALUE;
        }

        return $newValue;
    }
}
