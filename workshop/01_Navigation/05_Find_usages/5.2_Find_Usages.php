<?php
/**
 * Find Usages
 *
 * Alt+F7 (Windows/Linux/Mac OS X)
 *
 * Show Find Usages(팝업 사용)
 *
 * Ctrl+Alt+F7 (Windows/Linux)
 * Alt+Command+F7 (Mac OS X)
 *
 * 선택된 symbol 이 현재 프로젝트에서 사용된 위치들이 하이라이트된다.
 */

namespace Navigation5\JetBrains;

class FindUsages
{
    const MIN_VALUE = -3.5;
    const MAX_VALUE = 3.5;

    // 1. adjust() 에 caret 을 위치시키고 Find Usages 를 실행하자.
    //    tool window 가 열리면 그곳에서 사용된 곳을 확인하고 이동할 수 있다.
    //    이 tool window 에서는 필터링도 가능하다.
    //
    // 2. adjust() 에 caret 을 위치시키고 팝업을 이용한 Show Usages 를 실행해 보자
    public function adjust($original, $delta)
    {
        $newValue = $original + $delta;

        if ($newValue < self::MIN_VALUE) {
            $newValue = self::MAX_VALUE;
        }
        if ($newValue > self::MIN_VALUE) {
            $newValue = self::MAX_VALUE;
        }

        return $newValue;
    }
}
