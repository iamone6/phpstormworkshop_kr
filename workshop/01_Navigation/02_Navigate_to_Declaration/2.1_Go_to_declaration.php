<?php
/**
 * Go to Declaration
 *
 * 심볼의 정의 부분으로 이동합니다.
 *
 * Ctrl+Click or Ctrl+B (Windows/Linux)
 * Command+Click or Command+B (Mac OS X)
 */

namespace Navigation2\JetBrains;

use ArrayIterator;
use Navigation1\JetBrains\Customers\Customer;

$name = 'Hadi';
$age = 53;

// 1. Customer 심볼에서 Ctrl+Click (or Command+Click on Mac)
// 2. 키보드 단축키를 사용해 봅시다.
// 3. 아래의 constructor로 유입되는 변수 $age 의 정이 부분으로 이동합니다.
$person = new Customer($name, $age);

// 4. celebrateBirthday() 함수로 이동합니다.
// 5. $person variable가 정의된 곳으로 이동합니다.
$person->celebrateBirthday();

// 6. ArrayIterator SPL class의 정의로 이동합니다.
$myArray = array();
$myArrayIterator  = new \ArrayIterator($myArray);


