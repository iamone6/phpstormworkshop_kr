<?php
/**
 * Navigate to super class/method
 *
 * Ctrl+U (Windows/Linux)
 * Command+U (Mac OS X)
 *
 * Ctrl+H Show Hierarchy tool window (Windows/Linux/Mac OS X)
 *
 * 상위 클래스 및 메소드로 이동합니다.
 * (class hierarchy 의 위로 백워드 트래킹을 합니다)
 */

namespace Navigation4\JetBrains\Customers;

// 1. Customer 클래스에 caret 을 위치시키고 Navigate to Super Method 를 사용하여 hierarchy의 상위로 이동합니다.
class GoldCustomer extends Customer
{
    // 2. __construct 에 caret 을 위치시키고 Navigate to Super Method 를 사용하여 hierarchy의 위로 이동한다.
    // 3. Hierarchy tool window을 열고 클래스의 hierarchy 를 추적해 본다.
    //    (이 tool window 에서도 이동이 가능하다)
    /**
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name, 0.25);
    }
}
