<?php
/**
 * Navigate to Implementation
 *
 * Ctrl+Alt+B (Windows/Linux)
 * Alt+Command+B (Mac OS X
 *
 * 선택된 클래스의 구현부로 이동하기
 */

use Navigation4\JetBrains\Customers\Customer;
use Navigation4\JetBrains\Customers\GoldCustomer;

class GoToImplementation
{
    /**
     *
     */
    public function pleaseGoThere()
    {
        // 1.   caret 을 Customer 클래스에 위치시키고 구Navigate To Implementation을 실행하세요.
        //      Customer, SilverCustomer, GoldCustomer 를 각각 선택해 보세요.
        //      SilverCustomer, GoldCustomer 는 Customer 에서 파생된 것으로 간접적으로 구현된 것입니다.

        $customer = new Customer('Ford', 0);

        // 2. GoldCustomer 에 caret 을 위치시키고  Navigate To Implementation 을 실행해 보세요.
        $customer = new GoldCustomer('Arthur');
    }
}
