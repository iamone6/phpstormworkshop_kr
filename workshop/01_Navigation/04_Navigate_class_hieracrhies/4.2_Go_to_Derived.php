<?php
/**
 * Navigate to Implementation
 *
 * 에디터 왼쪽의 gutter(라인 수 표시 옆의 (I) 같은 것들)를 사용하여 interface/class 및 그 멤버의 구현부로 이동합니다.
 */

namespace Navigation4\JetBrains\Customers;

// 1. Customer 클래스의 구현부로 이동한 뒤에 되돌아 옵니다.
// 힌트 : Exercise 3.1 의 빠르게 되돌아오는 기능을 사용합니다.
interface ICustomer
{
    /**
     * @return double
     */
    public function getDiscount();

    /**
     * @return string
     */
    public function getName();

    /**
     * @param double $discount
     */
    public function setDiscount($discount);

    /**
     * @param string $name
     */
    public function setName($name);
}
