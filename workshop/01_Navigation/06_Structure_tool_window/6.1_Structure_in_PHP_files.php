<?php
/**
 * Structure Tool Window
 *
 * Alt+7 (Windows/Linux)
 * Command+7 s(Windows/Linux)
 *
 * Navigate to File Structure
 *
 * Ctrl+F12 (Windows/Linux)
 * Command+F12 (Mac OS X)
 *
 * file : classes, functions 의 구조도(아웃라인)를 디스플레이한다 (접근성을 보여주는 아이콘 포함)
 * HTML, JavaScript, CSS, ... 등등도 표시된다.
 *
 * 1. Structure Tool 도구를 열자.
 * 2. 방향키를 사용하여 fourthFunction() 으로 이동한다.
 *      (Enter 를 눌러 선택된 아이텡으로 이동한다)
 * 3.  File Structure popup 을 열고 HTML 섹션으로 이동한다.
 *     첫번째 구문으로 이동한다.
 */

namespace Navigation6\JetBrains;

class Class1
{
    /** @var int */
    protected $_classVariable;

    static function fourthFunction()
    {
    }

    public function firstFunction()
    {
    }

    protected function thirdFunction()
    {
    }

    private function secondFunction()
    {
    }
}

class Class2
{
    public function firstFunction()
    {
    }
}

$standardVariable = 1;
function standardFunction()
{
}

?>
<html>
<body>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis deserunt ea
    expedita magni omnis quis temporibus
    velit vitae. Ex laboriosam nesciunt nisi obcaecati possimus quia repellendus
    sequi similique soluta voluptas?</p>
</body>
</html>
