Remote Host

파일과 폴더를 로컬 프로젝트와 서버 사이에서 되돌렸다 다시 진행했다(이리저리 전송)를 해 보자.

0. docker-compose.yml 을 당신의 OS에 따라 특정 라인의 주석을 해제했다(고 가정한다).
0. Make sure that you've uncommented the line in docker-compose.yml according to your OS
1. "docker-compose up"을 실행했다(고 가정한다).
    이렇게 하면 SFTP 서버가 2022포트에서 시작하고 로컬 폴더(c:\tmp or /tmp)는 /home/jetbrains/upload 에 매핑될 것이다.
2.  Settings|Build, Execution, Deployment|Deployment 을 연다. vm 서버를 선택하고 패스워드를 제공한다 : jetbrains
3. _Search Everywhere_에서 _Remote Host_ tool 윈도우 를 찾는다.
5. remote host 를 브라우징하여 현재 프로젝트에서 원격 서버로 몇몇 파일들을 복사한다.
   이는 context 메뉴를 사용하거나 파일들을 drag&drop 하여 작업할 수 있다.