# Integration with VCS

source control 로 커밋할 때 파일을 서버로 전송하기.

1. 프로젝트 내 새 파일을 만들고 VCS에 포함시킨다.
2. 변경사항을 VCS에 커밋한다. _Commit Changes_ 다이얼로그내에서,_Upload files to_ 드롭다운 리스트 내의 서버를 선택한다. 
   파일들이 VCS에 커밋될 때 마다, PhpStrom은 원격 호스트에서 수정된 파일을 업로드하게 된다.