<?php
/**
 * Writing tests
 *
 * 새 파일을 만들고 PHPUnit 테스트를 선택한다.
 *
 * Alt+Insert or Ctrl+Shift+T (Go to Test) (Windows/Linux)
 * Command+N or Command+Shift+T (Go to Test) (Mac OS X)
 */

namespace Testing\JetBrains;

// 1. 새 PHP 파일을 만들자. 네비게이션 바에서 PHP template 를 사용해 새 파일을 생성한다.
//
// 2. 아래 코드를 추가한다.
//
//    namespace Testing\JetBrains;
//    use Testing\JetBrains\Queue;
//    class QueueTest extends TestCase {
//
//    }
//
// 3. setUp() 메소드를 하나 만들고 그 안에 $_queue 라는 이름으로 새로운 Queue 를 만든다.
//    setUp() 함수를 만들려면 Generate | Override method 액션을 사용한다(alt+Insert / Command+N)
//
//    이제 클래스는 다음과 같을 것이다:
//
//   class QueueTest extends TestCase {
//       /** @var Queue */
//       protected $_queue;
//
//       public function setUp() {
//           $this->_queue = new Queue();
//       }
//   }
//
//    Note: 위의 코드를 copy&patse 하려면 Code | Comment With Line Comment 메뉴가 편리할 것이다.
//
// 4. 첫번째 테스트로 enqueue() 메소드를 검증하는 테스트를 추가하자. (alt+Insert / command+N)
//
//  public function testEnqueueIncreasesItemCount() {
//      $this->_queue->enqueue('test');
//      $this->assertEquals(1, $this->_queue->getNumberOfItems());
//  }
//
// 5. Navigate to Test Subject 를 사용하여 테스트에서 Queue 클래스로 되돌아온다.
//    단축키는 Go to Test와 동일하다.
