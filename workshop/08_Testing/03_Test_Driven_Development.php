<?php
/**
 * Test-Driven Development (TDD)
 *
 * 테스트를 먼저 작성하고, 예상되는 결과를 대충 윤곽을 그린뒤에, 테스트중인 메소드를 구현하는 개발방법.
 *
 * Alt+Insert (Windows/Linux)
 * Command+N (Mac OS X)
 */

namespace Testing\JetBrains;

// 1. QueueTest.txt 에서 , 현재 존재하지 않는 메소드인 peek() 을 테스트하는 두개의 테스트를 추가한다.
//
//    public function testPeekReturnsNullWhenNoItemsInQueue() {
//        $result = $this->_queue->peek();
//        $this->assertNull($result);
//    }
//
//    public function testPeekReturnsItemWhenItemsInQueue() {
//        $this->_queue->enqueue('test');
//        $result = $this->_queue->peek();
//        $this->assertEquals('test',$result);
//    }
//
//    다시말하면 : 아이템들이 queue 에 있다면 peek() 는 item 를 리턴하고, 없다면 null 을 리턴한다.
//
// 2. PhpStorm displays an inspection on the peek() method: Method peek() not found in class Queue.
// 2. PhpStorm 은 peek() 메소드에 대한 내용을 표시한다 : Method peek() not found in class Queue.
//    Alt+Enter로 method 를 만들고
//    method 를 구현하자
// 3. unit tests 를 실행시키고 테스트를 모두 통과하는지 지켜보자
