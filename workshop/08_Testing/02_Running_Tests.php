<?php
/**
 * Running tests
 *
 * PHPUnit 으로 유닛 테스트를 실행한다. PHPUnit framework 은 PEAR나 composer 로 가져올 수 있다.
 */

namespace Testing\JetBrains;

// 1. PHPUnit 기반의 새로운 RUN Configuration 을 만들고 "Run tests" 로 명명한 뒤 테스트할 디렉토리를 아래 디렉토리로 명시하자.
//      \08_Testing\
//    특정한 클래스나 메소드만을 실행시킬 수 있다.
//    특정 실행 설정을 위한 PHPUnit XML configuration file 을 명시할 수도 있다.
// 2. PhpStorm 은 Settings | Languages & Frameworks | PHP | Test Frameworks | PHPUnit 에서 원격 인터프리터를 위한 PhpUnit 설정을
//    preConfigured 해 놓았으므로 Settings|PHP|CLI Interpreter 에서 인터프리터를 변경하기만 하면 된다.
// 3. 거의 다 왔다. 현재 Queue 클래스는 로딩될수 없다. 이를 해결하기 위해 아래 중 하나의 작업을 해야 한다.
//
//      require_once 'Code/Queue.php';  를 추가하기
//
//      또는
//
//      Settings | Languages & Frameworks | PHP | Test Frameworks | PHPUnit  에서
//      /opt/project/workshop/08_Testing/Code/Bootstrap.php 을 기본 bootstrap 파일로 설정하기
//      A bootstrap file 은 unit test runner 를 위한 커스텀 PHP코드를 실행하는 데에 쓰이며,
//      Bootstrap.php 는 테스트 실행에 필요한 모든 파일들을 담고 있다
// 4. Run Configuration 살행
// 5. Test tool window 하단에 테스트 결과가 나온다. context menu에서 number of assettions 같은 추가적인 통계도 볼 수 있다.
