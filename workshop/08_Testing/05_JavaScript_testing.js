/**
 * JavaScript Unit Testing (using Karma)
 * Karma 를 사용한 Javascript 유닛 테스트
 */

// 1. Karma 플러그인을 설치한다(IDE Settings | Plugins).
// 2. NPM으로 시스템에 Karma 테스트 런너를 설치한다.
//      npm install -g karma
//      (아니면 Setting Pane -> Languages & Frameworks | Node.js and NPM 을 열고 Karma 를 검색하여 -g 옵션과 함께 설치)
// 3. Karma configuration 이 생성되었는지 확인한다. karma.conf.js.sample 도 karma.conf.js 로 변경하거나 직접 설정한다.
//
//    직접 설정: 터미널을 열어 (Tools | Open Terminal) 현재 폴더에서 karma 를 초기화.
//      cd "08_Testing"
//      karma init
//
//    - 사용할 테스트 프레임워크를 선택. 우리는 jasmine 을 사용할 것이다
//    - Require.js 사용여부를 물어오면 "no"
//    - capture 할 브라우저 선택.
//    - test location 에서,  *.js 를 입력하여 Karma 가 현재 폴터에서 모든 테스트를 하도록 한다
//    - excluding files는, 빈 채로 둔다. Karma documentation 에 더 많은 내용이 있다.
//    - automatically running은  "no"를 선택. 이것이 활성화되면 코드나 테스트가 수정될때마다 자동으로 유닛 테스트를 실행한다
// 4. Karma 기반의 새로운 run configuration 을 만든다. 이름을 지어주고, Node.js 경로를 지정해 주면 karma.conf.js 가 생성되나
// 5. Run the configuration. Karma 는 브라우저을 열어서 아래에 정의된 유닛테스트를 실행한다. 테스트 결과는 IDE에 보여진다.

describe("Sample test suite", function() {
    it("a boolean true should be true", function() {
        expect(true).toBe(true);
    });
    it("a boolean false should be false", function() {
        expect(false).toBe(false);
    });
});
