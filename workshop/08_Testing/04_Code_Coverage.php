<?php
/**
 * Code Coverage
 *
 * 어떤 statement 가 테스트되고 어떤게 테스트되지 않는지를 체크
 */

namespace Testing\JetBrains;

// 1. Coverage configuration 과 함께 이전에 만든 QueueTest 를 실행한다.
//    PHPUnit 5+ 부터 필수적으로 whitelist 가 설정되어 있다.
// 2. Code Coverage tool window 에서 code coverage results 를 확인한다.
//    F4 나 더블클릭으로 Queue.php 로 가서 어떤 라인이 테스트되었고 아닌지를 확인한다.
